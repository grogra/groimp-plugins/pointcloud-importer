---
title: 'Pointcloud: Implementation of point clouds as graphs in the 3-d plant modeling platform GroIMP'
tags:
    - FSPM
    - Graph-rewriting
    - Pointclouds
    - Plantmodeling
    - GroIMP/ XL
authors:
  - name: Gaëtan Heidsieck
    orcid: 0000-0003-2577-4275
    equal-contrib: true
    affiliation: 1 
  - name: Tim Oberlaender
    equal-contrib: true
    affiliation: 1
  - name: Thomas Hay
    affiliation: 1
  - name: Winfired Kurth
    affiliation: 1
affiliations:
 - name: Georg-August-Universität Göttingen, Lower Saxony, Germany
   index: 1
date: 17 August 2024
bibliography: paper.bib
---

# Summary

The graph-based functional structural plant modeling (FSPM) platform GroIMP [@kniemeyer2008design] was extended by a set of functionalities for managing point clouds on the level of individual points using GroIMP's relational graph grammar.
These functionalities include the import of XYZ (simple x y z coordinates in CSV style) and PLY (sets of points, edges and faces) based point clouds as balanced trees to the GroIMP simulation graph and a set of basic point cloud manipulation tools. They are extended to support the possible faces and edges from the PLY as part of the point cloud.
We use this new implementation in GroIMP in two examples to showcase its usage: a model validation and a fine-grained light interception on meshes.


# Statement of need

In plant science, 3d point clouds are widely used as data structure as they are a straightforward way to represent a 3d object. They bring an efficient and fast approach to collect geometric data from plants/trees, which can be complex structures [@okura20223d]. This data can be used for extracting features [@GHAHREMANI2021106240], reconstructing topology [@rs5020491], [@preuksakarn2010reconstructing] or geometry meshes and volumes [@WANG2023103557], calibrating models, among others. FSPM represents plants by including fine grain properties and organ interactions in the plant growth representation. The FSPM representation benefits from the plant modeling and reconstruction approaches using point clouds, as they provide accurate analyses for model validation [@perez2023testing] or data-driven models. The point clouds are usually processed before being integrated into the FSP Model and used for the modeling. Abilities like simulated growth starting from the measured point-cloud, light simulation, or rule-based manipulation of the point cloud, are not yet fully explored. 

The platform GroIMP with its model-specification XL language provides queries for rewriting or retrieving data from graphs. The platform is well-adapted for plant modeling and has been used extensively for FSPM. In GroIMP, the models and data are represented as graphs. Thus, a graph representation of a point cloud eases its integration into the modeling process. This design also enables to leverage the XL language on the point clouds, allowing the usage of complex queries on both the point cloud and the model.

To showcase the possible benefit of such an approach we consider two use cases: validation of growth rules for a model to fit a measured tree, and integration of organs measured at high resolution into the model. In the case of validating the accuracy of a growth model simulating a measured tree, integrating the point cloud into the environment of the simulation eases the validation. A scenario where this kind of validation in GroIMP is used is the reconstruction of measured pine trees into a topological graph following a growth model. In the case of integrating measured organs (e.g. leaves) into a model, a suitable data integration of point clouds enables fine-grained analyses and manipulations of the organ. As PLY files can also contain faces, it would be possible to use a raytracer and gather the amount of intercepted energy for each face.
In a GroIMP model, this enables to use light models on measured leaves at a very precise resolution, while the leaf is connected to a growth model.

# PointCloud plugin in GroIMP

To import and export XYZ and PLY point clouds, new filters were created. Since these filters follow the general structure of importing and exporting in GroIMP, they can be used in any part of the software and in combination with other plugins.

The points of the imported point clouds are added as terminal vertices to a balanced tree in the project graph. Adding the objects only as leaves allowed us to keep the concept of local transformation of GroIMP, which enables the user to manipulate the whole point cloud at once using turtle geometry commands.

If the imported file also includes meshes or lines, these objects are added as separate trees. The meshes and lines are then linked to their corresponding points using refinement edges. This structure of separated trees with only specific relationships allows users to easily manipulate only one scale of the structure while keeping the included knowledge/relationships. 

For the visualization, the existing graph visitor that builds the 3d scene has been improved to speed up the overall visitation time. Indeed, by default, GroIMP's display visitors go through every node of the project graph for every user interaction. We added a possible cache of 3d objects that prevents re-visiting the same nodes. It is particularly impactful with point cloud graphs with millions of nodes.

A more in depth description of the point cloud objects in GroIMP is available on the software [wiki](https://wiki.grogra.de/doku.php?id=groimp-platform:pointcloud).

### Installation & usage

The code is available [on gitlab](https://gitlab.com/grogra/groimp-plugins/pointcloud), and can be used in GroIMP, either downloaded and compiled from the source, or directly from the [GroIMP plugin manager](https://wiki.grogra.de/doku.php?id=user-guide:pluginmanager&s[]=plugin&s[]=manager). Two example projects are embedded within the plugin and can be opened through the example explorer (File/Example projects). They represent the basic examples of the use cases described above.
More documentation on both the algorithms/implementation and on how to set up/use the plugin is available from the [wiki](https://wiki.grogra.de/doku.php?id=groimp-platform:pointcloud).

The example projects are used to highlight the features of the plugin. The first one uses a measured leaf organ in a PLY file (\autoref{fig:pc1}). The leaf is transformed and added several times to a branch model. A simple XL rewriting rule is used to simulate leaf growth, and the GPUFlux raytracer [@article] in the light model. The absorbed power per area is used to set the RGBAShader of each mesh in order to create a heatmap-like representation (\autoref{fig:pc1}). 


![Imported leaf added to a model branch. Results of light modeling. Each mesh is colored depending on its individual amount of light intercepted. \label{fig:pc1}](fig1.png){width="99%"}


The second example project shows how the point cloud can be integrated in model validation. The plant model is simulated in the same scene as the point cloud. The point's coordinates are tested for whether they fit in the volume of the simulated plant. The result of the fitting can be used in the growth rules (\autoref{fig:pc3}).

![Point cloud of a tree, and reconstruction models that matches the point cloud at 99 and 75 percents. \label{fig:pc3}](fig3.png)

## Acknowledgements 

We thank Maurice Müller and Nico Hundertmark for their help in the development of the plugin.

## References


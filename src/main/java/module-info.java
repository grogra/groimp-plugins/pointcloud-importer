module pointcloud {
	exports de.grogra.pointcloud.importer;
	exports de.grogra.pointcloud.objects;
	exports de.grogra.pointcloud.objects.impl;
	exports de.grogra.pointcloud.importer.ply;
	exports de.grogra.pointcloud.importer.xyz;
	exports de.grogra.pointcloud.utils;
	exports de.grogra.pointcloud.navigation;
	exports de.grogra.pointcloud.tools;

	requires java.logging;
	
	requires graph;
	requires platform.core;
	requires utilities;
	requires xl.core;
	requires platform;
	requires imp3d;
	requires imp;
	requires vecmath;
	requires rgg;
	requires math;
	requires mesh.renderer;
	
	requires java.desktop;
	
	requires jply;
	requires org.apache.commons.io;
}
package de.grogra.pointcloud.export.xyz;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point4d;
import de.grogra.graph.EdgePatternImpl;
import de.grogra.graph.Graph;
import de.grogra.graph.Path;
import de.grogra.graph.VisitorImpl;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.Cloud;
import de.grogra.imp3d.objects.CloudContext;
import de.grogra.imp3d.objects.PointCloud;
import de.grogra.pf.io.FileWriterSource;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.IO;
import de.grogra.pf.io.ObjectSource;
import de.grogra.pf.io.VirtualFileWriterSource;
import de.grogra.pf.io.WriterSource;
import de.grogra.pointcloud.export.ExportCloud;
import de.grogra.pointcloud.objects.CloudGraphBase;
import de.grogra.pointcloud.objects.CollectionCloud;
import de.grogra.pointcloud.objects.impl.LeafPointImpl;
import de.grogra.vfs.FileSystem;

/**
 * The source to export can be either a PointCloud object, or a CollectionCloud.
 */
public class XYZExportCloud extends ExportCloud implements FileWriterSource, VirtualFileWriterSource,
WriterSource {


	List<Float> coord;
	int batchSize = 10000;
	int b = 0;
	File outfile;
	Writer wout;
		
	
	public XYZExportCloud(FilterItem item, FilterSource source) {
		super(item, source);
	}

	/**
	 * Class used to export Cloud that are not part of the graph. This do not modify the Cloud.
	 */
	class PointCloudThrowAway extends PointCloud {
		Cloud cloud;
		PointCloudThrowAway(Cloud c) {
			this.cloud = c;
		}
		@Override public Cloud getCloud() { return cloud; }
		@Override public void setCloud(Cloud c) { }
	}
	
	@Override
	public void write(File out) throws IOException {
		outfile = out;
		coord = new ArrayList<Float>();
		
		List<PointCloud> pc = new ArrayList<PointCloud>();
		
		if (!(source instanceof ObjectSource)) { return; }
		Object o = ((ObjectSource)source).getObject();
		if (o instanceof CloudContext) {
			if (((CloudContext)o).getCloud().getNode() != null) {
				pc.add( (PointCloud) ((CloudContext)o).getCloud().getNode());
			} else {
				pc.add(new PointCloudThrowAway( ((CloudContext)o).getCloud())  );
			}
		} else if (o instanceof CollectionCloud) {
			PointCloud[] clouds = ((CollectionCloud)o).getClouds();
			pc.addAll(findCloudWithPoints(clouds));
		} else if (o instanceof CloudContext[]) {
			CloudContext[] array = (CloudContext[])o;
			for (int i = 0; i<array.length; i++) {
				if (((CloudContext)array[i]).getCloud().getNode() != null) {
					pc.add( (PointCloud) ((CloudContext)array[i]).getCloud().getNode());
				} else {
					pc.add(new PointCloudThrowAway( ((CloudContext)array[i]).getCloud())  );
				}
			}
		}
		if (pc.isEmpty()) { return; }

		BufferedWriter writer = new BufferedWriter(new FileWriter(outfile));
		writer.close();

		for (PointCloud n : pc) {
			export(n);
		}
		
		if (!coord.isEmpty()) {
			writeBufferedPos();
		}
		coord.clear();
	}
	
	@Override
	public void write(FileSystem fs, Object out) throws IOException {				
		write(fs.getWriter(fs.getFile(IO.toPath(getSystemId())), false));
	}
	
	/**
	 * Used for internal saving
	 */
	@Override
	public void write(Writer out) throws IOException {
		wout = out;
		coord = new ArrayList<Float>();
		
		List<PointCloud> pc = new ArrayList<PointCloud>();
		
		if (!(source instanceof ObjectSource)) { return; }
		Object o = ((ObjectSource)source).getObject();
		if (o instanceof CloudContext) { 
			// this ensure that the cloud is saved even if it has a Node that is 
			// not a PointCloud
			pc.add(new PointCloudThrowAway( ((CloudContext)o).getCloud())  );
		} else if (o instanceof CollectionCloud) {
			PointCloud[] clouds = ((CollectionCloud)o).getClouds();
			pc.addAll(findCloudWithPoints(clouds));
		}
		if (pc.isEmpty()) { return; }
		
		// for the internal export/import -> The local transformation are kept
		transformOfCloud = new Matrix4d();
		transformOfCloud.setIdentity();
		for (PointCloud n : pc) {
			export(n);
		}
		
		if (!coord.isEmpty()) {
			writeBufferedPos(wout);
		}
		coord.clear();
		wout.close();
	}
	
	@Override
	public void exportImpl(PointCloud pc)
	{
		if (pc.getCloud() instanceof CloudGraphBase) {
			ExportVisitor v = new ExportVisitor();
			v.init(((Node)pc).getCurrentGraphState(),  new EdgePatternImpl
					(Graph.BRANCH_EDGE | Graph.SUCCESSOR_EDGE, Graph.REFINEMENT_EDGE, true, true));
			((Node)pc).getGraph().accept((Node)pc, v, null);
		}
		else {
			for ( Cloud.Point p : pc.getCloud().getPoints()) {
				writePoint(p);
			}
		}
		
	}
	
	/**
	 * returns the first cloud with Point that are actual points (not meshes or lines).
	 * Returns null if not such cloud is found.
	 */
	private List<PointCloud> findCloudWithPoints(PointCloud[] clouds) {
		List<PointCloud> result = new ArrayList<PointCloud>();
		for (PointCloud pc : clouds) {
			if (pc.getCloud().peek().toFloat().length == 3) {
				result.add((PointCloud)pc);
			}
		}
		return result;
	}
	
	private void writePoint(Cloud.Point p) {
		float[] pos = p.toFloat();
//		if (!(pos.length%3==0)) { return;	}
		int i=0;
		float[] tmp = new float[3];
		for (float f : pos) {
			tmp[i]=f;
			i++;
			if (i>2) {
				Point4d point = new Point4d(tmp[0], tmp[1], tmp[2], 1 );
				transformOfCloud.transform(point);
				coord.add( (float)point.x );
				coord.add( (float)point.y );
				coord.add( (float)point.z );
				b++;
				i=0;
				if (b>=batchSize) {
					// write in file, close and reset b
					b=0; 
					if (wout != null) {
						try {
							writeBufferedPos(wout);
						} catch (IOException e) {
							e.printStackTrace();
						}
					} else {
						writeBufferedPos();
					}
					coord.clear();
				}
			}
		}
		
		
		
	}
	
	
	private void writeBufferedPos(Writer writer) throws IOException {
		int i = 0;
    	
	    for (Float d : coord) {
	    	if (i == 0) {
	    		writer.append(String.valueOf(d));
	    		i = 1;
	    	}
	    	else if (i == 1) {
	    		writer.append(" " + String.valueOf(d));
	    		i = 2;
	    	}
	    	else if (i == 2) {
	    		writer.append(" " + String.valueOf(d) + System.lineSeparator());
	    		i = 0;
	    	}
	    }
	}
	
	private void writeBufferedPos() {
		int i = 0;
	    try (BufferedWriter writer = new BufferedWriter(new FileWriter(outfile, true)) ) {
	    	
		    for (Float d : coord) {
		    	if (i == 0) {
		    		writer.append(String.valueOf(d));
		    		i = 1;
		    	}
		    	else if (i == 1) {
		    		writer.append(" " + String.valueOf(d));
		    		i = 2;
		    	}
		    	else if (i == 2) {
		    		writer.append(" " + String.valueOf(d) + System.lineSeparator());
		    		i = 0;
		    	}
		    }
	    } catch(IOException e) {
	    	e.printStackTrace();
	    }
	}

	class ExportVisitor extends VisitorImpl {
		ExportVisitor (){
			super();
		}
		@Override
		public Object visitEnter (Path path, boolean node)
		{
			if (super.visitEnter(path, node)!=null) {
				return STOP;
			}
			if (node)
			{
				Node n = (Node) path.getObject (-1);
				if (n instanceof LeafPointImpl) {
					writePoint((Cloud.Point)n);
				}
			}
			return null;
		}

		@Override
		public Object visitInstanceEnter ()
		{
			return STOP;
		}
	}
}

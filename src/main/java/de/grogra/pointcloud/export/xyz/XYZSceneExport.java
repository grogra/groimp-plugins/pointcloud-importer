package de.grogra.pointcloud.export.xyz;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point4d;
import de.grogra.graph.EdgePatternImpl;
import de.grogra.graph.Graph;
import de.grogra.graph.Path;
import de.grogra.graph.VisitorImpl;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.View3D;
import de.grogra.imp3d.objects.Cloud;
import de.grogra.imp3d.objects.CloudArray;
import de.grogra.imp3d.objects.PointCloud;
import de.grogra.imp3d.objects.SceneTree;
import de.grogra.imp3d.objects.SceneTree.InnerNode;
import de.grogra.imp3d.objects.SceneTree.Leaf;
import de.grogra.pf.io.FileWriterSource;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.FilterSource;
import de.grogra.pointcloud.export.ExportPointCloud;
import de.grogra.pointcloud.objects.CloudGraphBase;

public class XYZSceneExport extends ExportPointCloud implements FileWriterSource {

	List<Float> cord;
	int batchSize = 10000;
	int b = 0;
	File outfile;
	public static Matrix4d transformOfCloud;

	public XYZSceneExport(FilterItem item, FilterSource source) {
		super(item, source);
		setFlavor(item.getOutputFlavor()); 
	}

	@Override
	public void write(File out) throws IOException {
		cord = new ArrayList<Float>();
		outfile = out;
		BufferedWriter writer = new BufferedWriter(new FileWriter(out));
		writer.write("");
		writer.close();
		write ();
		if (!cord.isEmpty()) {
			writeBufferedPos();
		}
		cord.clear();
	}


	@Override
	protected SceneTree createSceneTree(View3D scene) {
		SceneTree t = new SceneTree (scene) {

			@Override
			protected boolean acceptLeaf(Object object, boolean asNode) {
				return getExportFor(object, asNode) != null;
			}
			@Override
			protected Leaf createLeaf(Object object, boolean asNode, long id) {
				Leaf l = new Leaf (object, asNode, id);
				init(l);
				return l;
			}
		};
		t.createTree(true,false);
		return t;
	}

	@Override
	public NodeExport getExportFor (Object object, boolean asNode)
	{
		if (!asNode) { return null; }
		if (object instanceof PointCloud) {
			return new ExpNode();
		}
		return null;
	}

	class ExpNode extends de.grogra.pointcloud.export.Leaf {
		@Override
		public void exportImpl(Leaf node, InnerNode transform, ExportPointCloud export) throws IOException {			
			Cloud pc = ((PointCloud) node.object).getCloud();
			transformOfCloud = this.getTransformation();

			if (pc instanceof CloudGraphBase) {
				ExportVisitor v = new ExportVisitor();
				v.init(pc.getNode().getCurrentGraphState(),  new EdgePatternImpl
						(Graph.BRANCH_EDGE | Graph.SUCCESSOR_EDGE, Graph.REFINEMENT_EDGE, true, true));
				pc.getNode().getGraph().accept(pc.getNode(), v, null);
			}
			else if (pc instanceof CloudArray) {
				Matrix4d t = this.getTransformation();
				for ( Cloud.Point p : pc.getPoints()) {
					writePoint(p);
				}
			}
		}
	}


	private void writePoint(Cloud.Point p) {
		float[] pos = p.toFloat();
		if (!(pos.length>2)) { return;	}
		Point4d point = new Point4d(pos[0], pos[1], pos[2], 1 );
		transformOfCloud.transform(point);
		cord.add( (float)point.x );
		cord.add( (float)point.y );
		cord.add( (float)point.z );
		b++;
		if (b>=batchSize) {
			// write in file, close and reset b
			b=0;
			writeBufferedPos();
			cord.clear();
		}
	}

	private void writeBufferedPos() {
		int i = 0;
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(outfile, true)) ) {

			for (Float d : cord) {
				if (i == 0) {
					writer.append(String.valueOf(d));
					i = 1;
				}
				else if (i == 1) {
					writer.append(" " + String.valueOf(d));
					i = 2;
				}
				else if (i == 2) {
					writer.append(" " + String.valueOf(d) + System.lineSeparator());
					i = 0;
				}
			}
		} catch(IOException e) {
			e.printStackTrace();
		}
	}

	class ExportVisitor extends VisitorImpl {
		ExportVisitor (){
			super();
		}
		@Override
		public Object visitEnter (Path path, boolean node)
		{
			if (super.visitEnter(path, node)!=null) {
				return STOP;
			}
			if (node)
			{
				Node n = (Node) path.getObject (-1);
				if (n instanceof Cloud.Point)	{
					writePoint((Cloud.Point)n);
				}
			}
			return null;
		}
	}
}

package de.grogra.pointcloud.export.ply;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point4d;
import javax.vecmath.Vector3f;

import de.grogra.graph.EdgePatternImpl;
import de.grogra.graph.Graph;
import de.grogra.graph.Path;
import de.grogra.graph.VisitorImpl;
import de.grogra.graph.impl.Edge;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.View3D;
import de.grogra.imp3d.objects.Cloud;
import de.grogra.imp3d.objects.CloudArray;
import de.grogra.imp3d.objects.Null;
import de.grogra.imp3d.objects.PointCloud;
import de.grogra.imp3d.objects.PolygonMesh;
import de.grogra.imp3d.objects.SceneTree;
import de.grogra.imp3d.objects.SceneTree.InnerNode;
import de.grogra.imp3d.objects.SceneTree.Leaf;
import de.grogra.pf.io.FileWriterSource;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.FilterSource;
import de.grogra.pointcloud.export.ExportPointCloud;
import de.grogra.pointcloud.objects.CloudGraphBase;
import de.grogra.pointcloud.objects.CollectionCloud;
import de.grogra.pointcloud.objects.PointCloudLeaf;
import de.grogra.pointcloud.objects.impl.LeafLineImpl;
import de.grogra.pointcloud.objects.impl.LeafMeshImpl;
import de.grogra.pointcloud.objects.impl.LeafPointImpl;

public class PLYSceneExport extends ExportPointCloud implements FileWriterSource {
	
	static String encoding = "ascii 1.0";

	// if the refinement edges that link the faces and edges to point where deleted by the user
	// these object then need to use their own coordinate to export
	boolean refinement_edges_clear = false;
	boolean refinement_edges_tested = false;
	Map<Long, float[]> vertices; // !! the first float IS ALWAYS the index in the MAP
	List<int[]> edges; // pair of vertex ids
	List<int[]> faces; // triple of vertex ids (only support triangle mesh for now

	static int currIndex = -1;
	static int currNodeIndex=0;

	Map<PointCloud, Matrix4d> pointclouds;

	public PLYSceneExport(FilterItem item, FilterSource source) {
		super(item, source);
		setFlavor(item.getOutputFlavor()); 
	}

	public String getHeader() {
		StringBuffer sb = new StringBuffer();
		sb.append("ply"+System.lineSeparator());
		sb.append("format "+encoding+System.lineSeparator());
		sb.append("comment Exported by GroIMP"+System.lineSeparator());
		sb.append("element vertex "+vertices.size()+System.lineSeparator());
		sb.append("property double x"+System.lineSeparator());
		sb.append("property double y"+System.lineSeparator());
		sb.append("property double z"+System.lineSeparator());
		if (!edges.isEmpty()) {
			sb.append("element edge "+edges.size()+System.lineSeparator());
			sb.append("property list uchar int vertex_indices"+System.lineSeparator());
		}
		if (!faces.isEmpty()) {
			sb.append("element face "+faces.size()+System.lineSeparator());
			sb.append("property list uchar int vertex_indices"+System.lineSeparator());
		}
		sb.append("end_header"+System.lineSeparator());
		return sb.toString();
	}

	@Override
	public void write(File out) throws IOException {
		pointclouds = new HashMap<PointCloud, Matrix4d>();
		// to call at every collections if multi files
		currIndex=0;
		vertices = new LinkedHashMap<Long, float[]> ();
		edges = new ArrayList<int[]>() ;
		faces = new ArrayList<int[]>() ;

		currNodeIndex = 0;
		
		// clear the file
		BufferedWriter writer = new BufferedWriter(new FileWriter(out));
		writer.close();

		//TODO: do not use the exportscenegraph visitor to get the collections
		write (); // this retrieve the collection cloud objects.

		if (pointclouds.isEmpty()) { return; }

		List<CollectionCloud> collections = new ArrayList<CollectionCloud>();
		
		for (PointCloud pc : pointclouds.keySet()) {
			if (((Node)pc).getAxisParent() instanceof CollectionCloud) {
				if (!collections.contains((CollectionCloud) ((Node)pc).getAxisParent())) {
					collections.add((CollectionCloud) ((Node)pc).getAxisParent());
				}
			} else {
				export(pc.getCloud(), false);
			}
		}
		
		
		//TODO: expect collection to only have CloudGraph as Cloud
		for (CollectionCloud pc : collections) {
			PointCloud[] clouds = pc.getClouds();
			Cloud[] bases = new Cloud[clouds.length];
			for (int i = 0; i<clouds.length;i++) {
				bases[i] = clouds[i].getCloud();
			}
			if (bases.length==0) { continue; }
			List<Class<? extends PointCloudLeaf>> types = getListOfChildrenType(bases);
			List<Integer> list_index = getPointClass(types);

			if (!list_index.isEmpty()) {
				for (int i : list_index) {
	//				refinement_edges_clear = wereRefinementEdgesCleared(bases, types, i);
//					if (!refinement_edges_clear) {
					export( bases[i], true );
//					}
				}
			} 
//			else { refinement_edges_clear = true; }
			for (int j = 0; j < bases.length; j++) {
				if (list_index.contains(j)) { continue; }
				refinement_edges_tested = false;
				export( bases[j], false);
			}
		}

		writer = new BufferedWriter(new FileWriter(out));
		writer.append(getHeader());
		writer.close();

		writeVertexes(out);
		writeEdges(out);
		writeFaces(out);
	}

	/**
	 * Get the next id to use as PLY id (the row number)
	 */
	int getNextId() {
		return currIndex++;
	}
	
	/**
	 * Get the next available id for a node 
	 */
	int getNextNodeId() {
		return currNodeIndex++;
	}

	/**
	 * Get a list of the class of each pointcloud base. The list is in the same order as the bases.
	 */
	public List<Class<? extends PointCloudLeaf>> getListOfChildrenType(Cloud[] bases){
		List<Class<? extends PointCloudLeaf>> res = new ArrayList<Class<? extends PointCloudLeaf>>();
		for (Cloud pc : bases) {
			try {
				if (pc instanceof CloudGraphBase) {
					res.add(((CloudGraphBase)pc).getChildrenType());
				} else {
					res.add(null);
				}
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		return res;
	}

	/**
	 * Check if the list of class include a class that extends PointCloudLeaf. 
	 * Returns a List of the index where the leaves extend PointLeaf. The 
	 * list is empty if no such cloud is given.
	 */
	public List<Integer> getPointClass(List<Class<? extends PointCloudLeaf>> types) {
		int i = 0;
		List<Integer> result = new ArrayList<Integer>();
		for (Class<? extends PointCloudLeaf> c : types) {
			if ( LeafPointImpl.class.isAssignableFrom(c)) {
				result.add(i);
			}
			i++;
		}
		return result;
	}

	/**
	 * In the case where the pointcloud includes edges and/or faces, 
	 * they are linked to the point leaves with refinement edges when 
	 * imported. To save space, the user can delete these edges. In that 
	 * case the points are not used for export. New points are created.
	 */
	public boolean wereRefinementEdgesCleared(PointCloudLeaf n) {
		if (refinement_edges_tested) {
			return refinement_edges_clear;
		}
		refinement_edges_tested = true;
		for (Edge e = n.getNode().getFirstEdge (); e != null; e = e.getNext (n.getNode()))
		{
			if (e.getSource() == n.getNode() && e.getTarget() instanceof PointCloudLeaf
					&& e.testEdgeBits(Graph.REFINEMENT_EDGE)) {
				return refinement_edges_clear = false;
			}
		}
				
		return refinement_edges_clear = true;
	}
//	public boolean wereRefinementEdgesCleared(Cloud[] bases, List<Class<? extends PointCloudLeaf>> types, int point_index) {
//		if (point_index < 0) { return true; } // there is no point leaves
//		if (bases.length <2) { return false; } // there is only the point leaves
//		for (int i = 0; i < bases.length; i++) {
//			if (i != point_index) {
//				PointCloudLeaf l = ((CloudGraphBase)bases[i]).getFirstLeaf();
//				if (l == null) { return false; }
//				for (Edge e = l.getNode().getFirstEdge (); e != null; e = e.getNext (l.getNode()))
//				{
//					if (e.getSource() == l.getNode() && e.getTarget() instanceof PointCloudLeaf
//							&& e.testEdgeBits(Graph.REFINEMENT_EDGE)) {
//						return false;
//					}
//				}
//			}
//		}
//		return true;
//	}
	
	public List<Node> getPointsFromRefinementEdges(PointCloudLeaf o){
		List<Node> points = new ArrayList<Node>();
		for (Edge e = o.getNode().getFirstEdge (); e != null; e = e.getNext (o.getNode()))
		{
			if (e.getSource() == o.getNode() && e.getTarget() instanceof LeafPointImpl
					&& e.testEdgeBits(Graph.REFINEMENT_EDGE)) {
				points.add(e.getTarget());
			}
		}
		return points;
	}
	
	void writeVertexes(File out) {
		if (vertices.isEmpty()) { return; }
	    try (BufferedWriter writer = new BufferedWriter(new FileWriter(out, true)) ) {
	    	for (float[] e : vertices.values()) {
	    		writer.append(e[1] + " " + e[2] + " " + e[3] + " " + System.lineSeparator());
	    	}
	    } catch(IOException e) {
	    	e.printStackTrace();
	    }
	}

	void writeEdges(File out) {
		if (edges.isEmpty()) { return; }
	    try (BufferedWriter writer = new BufferedWriter(new FileWriter(out, true)) ) {
	    	for (int[] e : edges) {
	    		writer.append(e.length + " " + e[0] + " " + e[1] + System.lineSeparator());
	    	}
	    } catch(IOException e) {
	    	e.printStackTrace();
	    }
	}

	void writeFaces(File out) {
		if (faces.isEmpty()) { return; }
	    try (BufferedWriter writer = new BufferedWriter(new FileWriter(out, true)) ) {
	    	for (int[] e : faces) {
	    		writer.append(e.length + " " + e[0] + " " + e[1] + " " + e[2] + System.lineSeparator());
	    	}
	    } catch(IOException e) {
	    	e.printStackTrace();
	    }
	}

	@Override
	protected SceneTree createSceneTree(View3D scene) {
		SceneTree t = new SceneTree (scene) {

			@Override
			protected boolean acceptLeaf(Object object, boolean asNode) {
				return getExportFor(object, asNode) != null;
			}
			@Override
			protected Leaf createLeaf(Object object, boolean asNode, long id) {
				Leaf l = new Leaf (object, asNode, id);
				init(l);
				return l;
			}
		};
		t.createTree(true,false);
		return t;
	}

	@Override
	public NodeExport getExportFor (Object object, boolean asNode)
	{
		if (!asNode) { return null; }
		if (object instanceof PointCloud) {
			return new ExpNode();
		}
		return null;
	}
	
	class ExpNode extends de.grogra.pointcloud.export.Leaf {
		@Override
		public void exportImpl(Leaf node, InnerNode transform, ExportPointCloud export) throws IOException {
			pointclouds.put((PointCloud) node.object, this.getTransformation());
		}
		
	}

	class ExportVisitor extends VisitorImpl {
		boolean onlyPoint;
		Matrix4d transform;
		
		ExportVisitor (boolean p, Matrix4d t){
			super();
			this.onlyPoint = p;
			this.transform = t;
		}
		@Override
		public Object visitEnter (Path path, boolean node)
		{
			if (super.visitEnter(path, node)!=null) {
				return STOP;
			}
			if (node)
			{
				Node n = (Node) path.getObject (-1);
				if (onlyPoint) {
					if (n instanceof LeafPointImpl) {
						Point4d v = new Point4d( ((Null)n).getTranslation() );
						transform.transform(v);
						vertices.put(n.getId(), new float[] {(float) getNextId(), (float) v.x,(float) v.y,(float) v.z } );
						currNodeIndex = (int) Math.max(n.getId()+1, currNodeIndex);
					}
				} else {
					if (n instanceof LeafPointImpl) {
						Point4d v = new Point4d( ((Null)n).getTranslation() );
						transform.transform(v);
						vertices.put(n.getId(), new float[] {(float) getNextId(), (float) v.x,(float) v.y,(float) v.z } );
						currNodeIndex = (int) Math.max(n.getId()+1, currNodeIndex);
					} else if (n instanceof LeafMeshImpl) {
						float[] vertexPos;
						int[] faceVertexes = new int[3];
						if (wereRefinementEdgesCleared((LeafMeshImpl)n)) { // use own coordinates
							vertexPos = ((PolygonMesh)((LeafMeshImpl)n).getPolygons()).getVertexData();
							if (vertexPos == null) { return null; }
							int nb_points = vertexPos.length/3;
							for (int i = 0; i < nb_points; i++) { // should be 3
								long id = (long) getNextId();
								Point4d pos = new Point4d(vertexPos[i*3],vertexPos[i*3+1],vertexPos[i*3+2], 1);
								transform.transform(pos);
								vertices.put((long)getNextNodeId(), new float[] {
										(float) id, (float) pos.x,(float) pos.y,(float) pos.z });
								faceVertexes[i] = (int) id;
							}
						} else { // look for the points
							int i = 0;
							for (Node p : getPointsFromRefinementEdges((PointCloudLeaf) n)) {
								vertexPos = vertices.get(p.getId());
								if (vertexPos == null) {
									Point4d v = new Point4d( ((Null)p).getTranslation() );
									transform.transform(v);
									vertexPos = new float[]{(float) getNextId()};
									vertices.put(p.getId(), new float[] {vertexPos[0], (float) v.x,(float) v.y,(float) v.z } );
									currNodeIndex = (int) Math.max(p.getId()+1, currNodeIndex);
								}
								if (vertexPos!=null) {
									faceVertexes[i] = (int) vertexPos[0];
								}
								i++;
							}
						}
						faces.add(faceVertexes);

					} else if (n instanceof LeafLineImpl) {
						float[] vertexPos;
						int[] edgeVertexes = new int[2];
						if (wereRefinementEdgesCleared((LeafLineImpl)n)) { // use own coordinates
							Point4d v = new Point4d( ((LeafLineImpl)n).getTranslation() );
							long id = (long) getNextId();
							transform.transform(v);
							vertices.put(id, new float[] {(float) id, (float) v.x,(float) v.y,(float) v.z } );
							Vector3f tup = ((LeafLineImpl)n).getAxis();
							Point4d v2 = new Point4d(tup.x, tup.y, tup.z, 1 );
							edgeVertexes[0] = (int) id;
							id = (long) getNextId();
							transform.transform(v2);
							vertices.put((long)getNextNodeId(), 
									new float[] {(float) id, (float) v2.x, (float)v2.y, (float)v2.z } );
							edgeVertexes[1] = (int) id;

						} else { // look for the points
							int i = 0;
							for (Node p : getPointsFromRefinementEdges((PointCloudLeaf) n)) {
								vertexPos = vertices.get(p.getId());
								if (vertexPos!=null) {
									edgeVertexes[i] = (int) vertexPos[0];
								}
								i++;
							}
						}
						edges.add(edgeVertexes);
					}
				}
			}
			return null;
		}

		@Override
		public Object visitInstanceEnter ()
		{
			return STOP;
		}
	}

	/**
	 * Export the given cloud to the lists: vertexes, faces, edges.
	 * If exportPoint is true, it only export the vertexes and fill an hashmap with 
	 * the node id and the vertex. Map used to map the points with the faces and 
	 * edges.
	 */
	public void export(Cloud pc, boolean exportPoint) {
		if (pc instanceof CloudGraphBase) {
			ExportVisitor v = new ExportVisitor(exportPoint, getTransform(pc));
			v.init(pc.getNode().getCurrentGraphState(),  new EdgePatternImpl
					(Graph.BRANCH_EDGE | Graph.SUCCESSOR_EDGE, Graph.REFINEMENT_EDGE, true, true));
			pc.getNode().getGraph().accept(pc.getNode(), v, null);
		}
		else if (pc instanceof CloudArray) {
			Matrix4d transform = getTransform(pc);
			for (Cloud.Point p : pc.getPoints()) {
				Point4d point = new Point4d(p.toFloat()[0], p.toFloat()[1], p.toFloat()[2], 1);
				transform.transform(point);
				vertices.put((long)getNextNodeId(), new float[] {getNextId(), (float) point.x, (float)point.y, (float)point.z});
			}
		}
	}
	
	/**
	 * Fetch the transform by resolving the arrya path of transform instead of GlobalTransform
	 * because the attribute globaltransf might not be up to date
	 */
	public Matrix4d getTransform(Cloud c) {
		return pointclouds.get(c.getNode());
	}

}

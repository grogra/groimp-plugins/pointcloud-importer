package de.grogra.pointcloud.ui;

import java.awt.Component;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * A class providing methods to create and show custom input dialogs
 * 
 * @author Nico Hundertmark
 */
public class UIDialog {
	/**
	 * The static instance of this class.
	 */
	private static UIDialog instance;
	
	/**
	 * Returns the static instance of this class. If the instance is null a new instance will be created.
	 * @return instance of this class
	 */
	public static UIDialog getInstance() {
		if(instance == null) {
			instance = new UIDialog();
		}
		return instance;
	}
	
	/**
	 * Displays a JPanel as a dialog.
	 * @param panel. The JPanel with will be displayed.
	 * @param title. The title of the dialog
	 * @param buttons. The button types
	 * @param type. The type of messagebox
	 * @return an integer indicating the option selected by the user
	 */
	public int displayParameterInput(JPanel panel, String title, int buttons, int type)  {
		int result = -1;
		if(!GraphicsEnvironment.isHeadless()) {
			result = JOptionPane.showConfirmDialog(null, panel, title, buttons, type);
		}
		return result;
	}
	
	/**
	 * Returns a dynamically created JPanel containing labels and components for inputting data.
	 * 
	 * @param labels. List of labels to be displayed
	 * @param types. List of types to determine the input component
	 * @return panel 
	 */
	public static JPanel createDynamicInputPanel(List<String> labels, List<Class<?>> types) {
        if (labels.size() != types.size()) {
            throw new IllegalArgumentException("Labels and types must have the same length.");
        }

        JPanel panel = new JPanel(new GridLayout(labels.size(), 2, 5, 5));
        
        for (int i = 0; i < labels.size(); i++) {
            panel.add(new JLabel(labels.get(i)));

            if (types.get(i) == Boolean.class) {
                panel.add(new JCheckBox());
            } else {
                panel.add(new JTextField());
            }
        }

        return panel;
    }
	
	/**
	 * Returns a list of objects representing the values of Textfields or checkboxes on a JPanel.
	 * The values are converted to the respective types.
	 * 
	 * @param panel. The panel containing the filled textboxes and checkboxes
	 * @param types. List of classes to convert the textbox values
	 * @return A List of values
	 */
	public static List<Object> getInputValues(JPanel panel, List<Class<?>> types) {
        List<Object> values = new ArrayList<>();
        Component[] components = panel.getComponents();

        for (int i = 1; i < components.length; i += 2) { // Input components are in odd indices
            Component component = components[i];
            Class<?> type = types.get((i - 1) / 2);

            if (type == String.class) {
                values.add(((JTextField) component).getText());
            } else if (type == Integer.class) {
                try {
                    values.add(Integer.parseInt(((JTextField) component).getText()));
                } catch (NumberFormatException e) {
                    values.add(null);
                }
            } else if (type == Double.class) {
                try {
                    values.add(Double.parseDouble(((JTextField) component).getText()));
                } catch (NumberFormatException e) {
                    values.add(null);
                }
            } else if (type == Boolean.class) {
                values.add(((JCheckBox) component).isSelected());
            } else {
                values.add(null); // Handle other types as needed
            }
        }

        return values;
    }
}

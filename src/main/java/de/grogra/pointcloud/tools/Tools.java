package de.grogra.pointcloud.tools;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

import de.grogra.graph.impl.Edge;
import de.grogra.imp3d.objects.Cloud;
import de.grogra.imp3d.objects.CloudArray;
import de.grogra.imp3d.objects.CloudContext;
import de.grogra.imp3d.objects.Plane;
import de.grogra.pointcloud.objects.CloudGraphBase;
import de.grogra.pointcloud.objects.impl.CloudGraph;
import de.grogra.pointcloud.objects.impl.CloudList;
import de.grogra.pointcloud.utils.Utils;
import de.grogra.xl.lang.Aggregate;
import de.grogra.xl.util.ObjectList;

public class Tools {

	
	public static Cloud merge(Aggregate a, CloudContext value) {
		if (a.initialize ())
		{
			a.aval1 = new ObjectList ();
		}
		if (a.isFinished ())
		{
			if (!(((ObjectList) a.aval1).size > 1)) { return null; }
//			Class cloudType = ((ObjectList) a.aval1).get(0).getClass();
//			Object[] array;
//			if (allCastableTo(cloudType, (ObjectList) a.aval1)) {
//				array = (Object[]) Array.newInstance (((ObjectList) a.aval1).get(0).getClass(), ((ObjectList) a.aval1).size ());
//			} else {
//				array = new Cloud[((ObjectList) a.aval1).size ];
//			}
			Object[] array = new Object[((ObjectList) a.aval1).size];
			int i = 0;
			for (Object o : ((ObjectList) a.aval1)) {
				array[i] = (Cloud)o;
				i++;
			}

			a.aval = merge(array);
		}
		else
		{
			((ObjectList) a.aval1).addIfNotContained(value.getCloud());
		}
		return null;
	}
	
	public static boolean allCastableTo(Class from, Iterable<Object> list) {
		boolean result = true;
		for (Object o : list) {
			if (!from.isAssignableFrom(o.getClass())) {
				return false;
			}
		}
		return result;
	}
	
	
	
	/**
	 * How ugly this is :o. But according to java specification MET51-J, the ambiguity should 
	 * be solved at compile time.
	 */
	public static Cloud merge(Object[] cs) {
		cs = assignCloudType(cs);
		if (cs instanceof CloudList[]) {
			return merge((CloudList[])cs);
		}
		if (cs instanceof CloudArray[]) {
			return merge((CloudArray[])cs);
		}
		if (cs instanceof CloudGraph[]) {
			return merge((CloudGraph[])cs);
		}
		if (cs instanceof Cloud[]) {
			return merge((Cloud[])cs);
		}
		return null;
	}
	
	/**
	 * find the index of the cloud whose point have the smallest dimension.
	 * e.g. if Cloud[] contains : a cloud of point(x,y,z) and a cloud of 
	 * meshes(x,y,z *3) the return is the index of the first cloud.
	 */
	public static int findSmallestDimesionCloud(Cloud[] cs) {
		int i = 0;
		int best = -1;
		int d = Integer.MAX_VALUE;
		for (Cloud c : cs) {
			if (c instanceof CloudArray) {
				if (d > 3) {
					d=3;
					best = i;
				}
			}
			else if (c instanceof CloudGraphBase) {
				int cur = ((CloudGraphBase)c).getFirstLeaf().toFloat().length;
				if (d > cur) {
					d=cur;
					best = i;
				}
			}
			i++;
		}
		return best;
	}
	
	/**
	 * This takes an array of Cloud element, and return an array of the best
	 * Cloud Type possible.
	 */
	private static Object[] assignCloudType(Object[] clouds) {
		if (clouds.length<1)  {return new Cloud[0];}
		if (clouds[0]==null) {return new Cloud[0];}
		Class cloudType = clouds[0].getClass();
		Object[] array;
		if (Tools.allCastableTo(cloudType, Arrays.stream(clouds).collect(Collectors.toList()))) {
			array = (Object[]) Array.newInstance (cloudType, clouds.length);
		} else {
			array = new Cloud[clouds.length ];
		}
		int i = 0;
		for (Object o : clouds) {
			array[i] = (Cloud)o;
			i++;
		}	
		return array;
	}
	

	/**
	 * Default merge on Cloud.
	 */
	public static Cloud merge(Cloud[] cs) {
		if(cs.length<=0) {
			return null;
		}
		int i = findSmallestDimesionCloud(cs);
		Cloud c1 = cs[i];
		Cloud[] clouds = new Cloud[cs.length-1];
	    System.arraycopy(cs, 0, clouds, 0, i);
	    System.arraycopy(cs, i+1, clouds, i, cs.length - i-1);

		return merge(c1,clouds);
	}
	public static Cloud merge(Cloud c1 , Cloud[] cs) {
		for(int i=0; i<cs.length;i++) {
			merge(c1,cs[i]);
		}
		return c1;
	}
	/***
	 * Merges the leaf nodes of the specified point cloud into the current point cloud.
	 * c2 is deleted.
	 */
	public static Cloud merge(Cloud c1, Cloud c2) {
		c1.addPoints(c2.getPoints());
		return c1;
	}
	
	/**
	 * Merge specifically CloudArrays, the arrays of points are copied with System.arraycopy
	 */
	public static Cloud merge(CloudArray[] cs) {
		if(cs.length<=0) {
			return null;
		}
		CloudArray c1 = cs[0];
		return merge(c1,Arrays.copyOfRange(cs, 1, cs.length));
	}	
	public static Cloud merge(CloudArray c1 , CloudArray[] cs) {
		for(int i=0; i<cs.length;i++) {
			merge(c1,cs[i]);
		}
		return c1;
	}
	public static Cloud merge(CloudArray c1, CloudArray c2) {
		float[] f = new float[ c1.pointsToFloat().length + c2.pointsToFloat().length ];
		System.arraycopy(c1.pointsToFloat(), 0, f, 0, c1.pointsToFloat().length);
		System.arraycopy(c2.pointsToFloat(), 0, 
				f, c1.pointsToFloat().length, c2.pointsToFloat().length);
		c1.setPoints(f);
		return c1;
	}
	
	/**
	 * Merge specifically Cloud List. The List of points are merged.
	 */
	public static Cloud merge(CloudList[] cs) {
		if(cs.length<=0) {
			return null;
		}
		CloudList c1 = cs[0];
		return merge(c1,Arrays.copyOfRange(cs, 1, cs.length));
	}	
	public static Cloud merge(CloudList c1 , CloudList[] cs) {
		for(int i=0; i<cs.length;i++) {
			merge(c1,cs[i]);
		}
		return c1;
	}
	public static Cloud merge(CloudList c1, CloudList c2) {
		c1.addPoints( c2.getPoints());
		return c1;
	}
	
	/**
	 * Merge specifically Cloud Graphs. The first children (Nodes) are simply moved 
	 * from one PointCloud Parent to another.
	 */
	public static Cloud merge(CloudGraphBase[] cs) {
		if(cs.length<=0) {
			return null;
		}
		int i = findSmallestDimesionCloud(cs);
		CloudGraphBase c1 = cs[i];
		CloudGraphBase[] clouds = new CloudGraphBase[cs.length-1];
	    System.arraycopy(cs, 0, clouds, 0, i);
	    System.arraycopy(cs, i+1, clouds, i, cs.length - i-1);

		return merge(c1,clouds);
	}
	public static Cloud merge(CloudGraphBase c1 , CloudGraphBase[] cs) {
		for(int i=0; i<cs.length;i++) {
			merge(c1,cs[i]);
		}
		return c1;
	}
	/**
	 * This breaks the pointcloud management. The points are not monitored when
	 * their are removed or added ...
	 */
	public static Cloud merge(CloudGraphBase c1, CloudGraphBase c2) {
		for (Edge e = c2.getNode().getFirstEdge (); e != null; e = e.getNext (c2.getNode()))
		{
			if (e.getSource() == c2.getNode() && e.getTarget() != c2.getNode()) {
				c1.getNode().addEdgeBitsTo(e.getTarget(), e.getEdgeBits(), e.getTarget().getGraph().getActiveTransaction());
				e.remove(e.getTarget().getGraph().getActiveTransaction());
			}
		}
		((CloudGraph)c1).addSize(((CloudGraph)c1).getSize());
		c2.dispose(true);
		return c1;
	}
	

	/**
	 * Split the could into two based on the selection of nodes.
	 * If some nodes do not belong to the cloud, they are ignored.
	 * The return array is [oldcloud, newcloud]
	 */
	public static Cloud[] split(Cloud.Point[] nodes, Cloud cloud) {
		if (cloud == null) { return new Cloud[2]; }
		cloud = Utils.ensureEditable(cloud);
		Cloud newCloud = null;
		if (cloud != null) {
			newCloud = cloud.createNew();
			newCloud.addPoints(cloud.remove(nodes));
		} 
		return new Cloud[] {cloud, newCloud};
	}
	
	
	public static Cloud[] split(Aggregate a, Cloud.Point p, Cloud parent) {
		if (a.initialize ())
		{
			a.aval1 = new ArrayList<Cloud.Point> ();
			a.aval2 = parent;
		}
		if (a.isFinished ())
		{		
			a.aval = split( ((ArrayList<Cloud.Point>)a.aval1).toArray(new Cloud.Point[0]), (Cloud)a.aval2);
		}
		else
		{
			((ArrayList<Cloud.Point>) a.aval1).add (p);
		}
		return null;
	}

	/***
	 * Split the cloud based on a plane. A new Cloud is created to have the 
	 * discarded Points. 
	 * The plane is pushed to the inverse of the global tranformation of the 
	 * cloud to be in the same relative coordinate.
	 * Return [oldcloud, newcloud]
	 */
	public static Cloud[] split(Plane plane, Cloud cloud) {
		cloud = Utils.ensureEditable(cloud);
		Cloud newCloud = cloud.createNew();
		for (Cloud.Point p : cloud.getPoints()) {
			if (!de.grogra.pointcloud.utils.Utils.isPointOverPlane(p, plane)) {
				Cloud.Point pr = cloud.remove(p);
				newCloud.addPoint(pr);
			}
		}
		return new Cloud[] {cloud, newCloud};
	}
	
	
	public static Cloud convert(Cloud old, Class newtype) {
		if (newtype.equals(old.getClass())) {
			return old;
		}
		Cloud c=null;
		if (newtype.equals(CloudArray.class)) {
			c = new CloudArray(old.pointsToFloat());
		}
		else if(newtype.equals(CloudList.class)) {
			c = new CloudList(old.pointsToFloat());
		}
		else if(newtype.equals(CloudGraph.class)) {
			c = new CloudGraph(old.pointsToFloat()); 
		}
		old.dispose(false);
		return c;
	}

//	/**
//	 * Aggregate the given Points into an new Cloud object. If they belong to a Node, 
//	 * their ownership is changed. Use the cloud type of the first Point if not null.
//	 * Use CloudGraph otherwise.
//	 */
//	public static Cloud toNewCloud(Cloud.Point[] p) {
//		Cloud newCloud = null;
//		if (!(p.length > 0)){ return new CloudList(); }
//		if (p[0].getCloud() != null) {
//			newCloud = p[0].getCloud().createNew();
//		}
//		return toNewCloud(p, newCloud);
//		
//		if (cloud != null) {
//			newCloud = cloud.createNew();
//			newCloud.addPoints(cloud.remove(nodes));
//		} 
////		else {
////			if (!(nodes.length >1)) { return new Cloud[2]; }
////			newCloud = nodes[0].getCloud().createNew();
////			for (Cloud.Point p : nodes) {
////				newCloud.addPoint(p.getCloud().remove(p));
////			}
////		}
//	}
//	
//	/**
//	 * Create a new Cloud of the given type and add all Poitns p. If the points already had a parent, change
//	 * their ownership. 
//	 * return the newly created Cloud.
//	 */
//	public static Cloud toNewCloud(Cloud.Point[] p, Cloud type) {
//		Cloud newCloud = null;
//		if (!(p.length > 0)){ return new CloudList(); }
//		if (type != null) {
//			newCloud = type.createNew();
//		} else {
//			if (p[0].getCloud() != null) {
//				newCloud = p[0].getCloud().createNew();
//			} else {
//				newCloud = new CloudGraph(new float[0]);
//			}
//		}
//		newCloud.setPoints(p);
//	}

}

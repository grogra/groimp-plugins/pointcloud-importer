package de.grogra.pointcloud.tools;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import javax.vecmath.Vector3d;

import de.grogra.imp3d.objects.Cloud;
import de.grogra.pointcloud.utils.Utils;

public abstract class KMeans {
	/***
	 * Clusters a given point cloud with given parameters
	 * @param pc point cloud to be clustered
	 * @param k The amount of clusters the point cloud should be split
	 * @param maxIterations The maximum iterations of the algorithm
	 * @return Array of point clouds representing the cluster
	 */
	public static Cloud[] cluster(Cloud pc, int k, int maxIterations){
		pc = Utils.ensureEditable(pc);
		List<Vector3d> centroids = new ArrayList<>();
		Random rand = new Random();
		Cloud.Point[] points = pc.getPoints();
//		List<Node> points = de.grogra.pointcloud.utils.Utils.getLeafNodes(pc);
		int[] clusters = new int[points.length];
		Arrays.fill(clusters, -1);
		
		for(int i = 0; i<k; i++) {
			Vector3d centroid = pointToVector3d(points[rand.nextInt(points.length-1)]);
			centroids.add(centroid);
		}
		
		int[] clusterSizes = new int[k];
		
		for(int iteration = 0; iteration < maxIterations; iteration++) {
			boolean changed = false;
			
			for(int i = 0; i < points.length; i++) {
				Cloud.Point leaf = points[i];
				
				double minDistance = Double.MAX_VALUE;
				int closestCentroid = -1;
				for(int j = 0; j < centroids.size(); j++) {
					double distance = distance(pointToVector3d(leaf), centroids.get(j));
					if(distance < minDistance) {
						minDistance = distance;
						closestCentroid = j;
					}
				}
				if(clusters[i] != closestCentroid) {
					//dynamicly update the centroids
					if(clusters[i] != -1) {
						//remove the point from the old cluster
						Vector3d oldCentroid = centroids.get(clusters[i]);
						Vector3d oldSum = new Vector3d(oldCentroid);
                        oldSum.scale(clusterSizes[clusters[i]]);
                        oldSum.sub(pointToVector3d(leaf));
                        clusterSizes[clusters[i]]--;
                        if (clusterSizes[clusters[i]] > 0) {
                            oldSum.scale(1.0 / clusterSizes[clusters[i]]);
                            centroids.set(clusters[i], oldSum);
                        }
					}
					
					// Add the point to the new centroid
                    clusters[i] = closestCentroid;
                    Vector3d newCentroid = centroids.get(closestCentroid);
                    Vector3d newSum = new Vector3d(newCentroid);
                    newSum.scale(clusterSizes[closestCentroid]);
                    newSum.add(pointToVector3d(leaf));
                    clusterSizes[closestCentroid]++;
                    newSum.scale(1.0 / clusterSizes[closestCentroid]);
                    centroids.set(closestCentroid, newSum);
                    
                    changed = true;
				}
			}
			
			if(!changed)
				break;
			
		}
		
		Cloud[] clusterList = new Cloud[k];
		for(int i = 0; i < k; i++) {
			clusterList[i] = pc.createNew();
		}
		for(int i = 0; i < points.length; i++) {
			//remove point from Graph
			pc.remove(points[i]);
//			points.get(i).remove(null);
			clusterList[clusters[i]].addPoint(points[i]);
		}
		
		return clusterList;
	}

	private static Vector3d pointToVector3d(Cloud.Point p) {
		float[] f = p.toFloat();
		if (f.length<3) { return null; }
		return new Vector3d( new double[] {(double)f[0], (double)f[1], (double)f[2] });
	}
	
	/**
	 * Calculates the euclidian distance form two vectors
	 * @param v1 first vector 
	 * @param v2 second vector
	 * @return the distance between
	 */
	private static double distance(Vector3d v1, Vector3d v2) {
		return Math.sqrt(Math.pow(v1.x - v2.x, 2) +
				Math.pow(v1.y - v2.y, 2) +
				Math.pow(v1.z - v2.z, 2));
	}
}

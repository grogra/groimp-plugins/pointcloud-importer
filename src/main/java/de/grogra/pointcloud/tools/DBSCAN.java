package de.grogra.pointcloud.tools;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.grogra.imp3d.objects.BoundedCloud;
import de.grogra.imp3d.objects.Cloud;
import de.grogra.pointcloud.utils.Utils;

public class DBSCAN {

	/***
	 * Clusters the given point cloud with the DBSCAN algorithm by the given parameters
	 * @param pc point cloud to be clustered
	 * @param epsilon The maximum distance for two points that are in the same
	 * cluster
	 * @param minimumNeighbours The minimum number of neighbors that must exist
	 * for a point to be no noise-cluster point.
	 * @param octreeDepth The number of layers in the octree that is used to
	 * cluster the point cloud. This must be > 1 to decrease the runtime of the
	 * clustering algorithm.
	 * @param removeNoiseCluster The indicator that can be set to true to remove
	 * all points that are not explicitly in an other point cloud.
	 * @return A list of point clouds representing the cluster
	 */
	public static Cloud[] cluster(BoundedCloud pc, double epsilon, int minimumNeighbours, int octreeDepth, boolean removeNoiseCluster){
		pc = (BoundedCloud) Utils.ensureEditable(pc);
		List<Cloud> clusters = new ArrayList<>();
		List<Cloud.Point> points = getListOfArrayPoint(Arrays.asList( pc.getPoints() ));
		Octree octree = new Octree(pc, octreeDepth);
		Cloud noise = pc.createNew();
//		PointCloudBaseImpl noise = new PointCloudBaseImpl("", pc.getBalancindFactor(), pc.getDepth(), LeafPointImpl.class);

		for(int i = 0; i < points.size(); i++) {
			Cloud.Point curr = points.get(i);
			List<Cloud.Point> neighbourList = octree.getNeighbors(curr, epsilon);
			if(neighbourList.size() < minimumNeighbours) {
				curr.getCloud().remove(curr);
				noise.addPoint(curr);
				continue;
			}
//			PointCloudBaseImpl cluster = new PointCloudBaseImpl("", pc.getBalancindFactor(), pc.getDepth(), LeafPointImpl.class);
			BoundedCloud cluster = (BoundedCloud) pc.createNew();
			List<Cloud.Point> neighbors = new ArrayList<Cloud.Point>();
			neighbors.add(curr);
			addNeighborsToCluster(octree, cluster, neighbors, epsilon);
			clusters.add(cluster);
		}
		if(!removeNoiseCluster) {
			clusters.add(noise);
		}
		Cloud[] res = new Cloud[clusters.size()];
		int i = 0;
		for (Cloud c : clusters) {
			res[i] = c;
			i++;
		}
		
		return res;
	}

	static private List<Cloud.Point> getListOfArrayPoint(List<Cloud.Point> points) {
		List<Cloud.Point> l = new ArrayList<Cloud.Point>();
		for (Cloud.Point p : points) {
			if (p instanceof Cloud.Point) {
				l.add((Cloud.Point) p);
			}
//			else {
//				l.add(new ArrayPoint(p.getCloud(), p.toFloat()));
//			}
		}
		return l;
	}	
	/**
	 * Searches for all points in the octree that are neighbored to points of
	 * the given neighbors list and adds all neighbored points to that list and
	 * the current point cloud. Points are neighbored if the distance (epsilon)
	 * between both points is lower than the given epsilon parameters.
	 *
	 * @param octree The octree to get the points from
	 * @param pointCloud The point cloud to put the points to
	 * @param neighbors The list of currently checked points
	 * @param epsilon The maximum distance that must be between two points to be
	 * neighbored
	 */
	private static void addNeighborsToCluster(Octree octree, BoundedCloud pointCloud, List<Cloud.Point> neighbors, double epsilon) {
		if (neighbors.size() == 0) {
			return;
		}
		Cloud.Point point = neighbors.get(0);
		List<Cloud.Point> currentNeighbors = octree.getNeighbors(point, epsilon);
		neighbors.remove(0);
//		point.getLeaf().remove(null);
//		pointCloud.addPoint(point.getLeaf());
//		point.setPointCloud(pointCloud);
		pointCloud.addPoint(point);
		int index = 0;
		while (index < currentNeighbors.size()) {
			Cloud.Point neighbor = currentNeighbors.get(index);
			if (neighbor.getCloud() == null) {
				pointCloud.addPoint(neighbor);
//				neighbor.setCloud(pointCloud);
				neighbors.add(neighbor);
				addNeighborsToCluster(octree, pointCloud, neighbors, epsilon);
			}
			index++;
		}
	}
}

//package de.grogra.pointcloud.clustering;
//
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;
//
//import javax.vecmath.Matrix4d;
//
//import de.grogra.graph.impl.Node;
//import de.grogra.imp3d.objects.MeshNode;
//import de.grogra.imp3d.objects.Null;
//import de.grogra.imp3d.objects.Sphere;
//import de.grogra.pf.registry.Item;
//import de.grogra.pf.ui.Context;
//import de.grogra.pointcloud.groimp.ArrayPoint;
//import de.grogra.pointcloud.groimp.Cloud;
//import de.grogra.pointcloud.groimp.Imp3dCloud;
//import de.grogra.pointcloud.groimp.PointCloud;
//import de.grogra.pointcloud.groimp.Utils;
//import de.grogra.pointcloud.objects.impl.CloudList;
//
///**
// * This class provides lots of methods to fit objects to point clouds.
// * Currently, spheres, cylinders, frustums and cones are supported. As an extra
// * feature, an automatic mode can be used. The automatic mode automatically
// * chooses the object with the best score. For all fitting methods, fibonacci
// * spheres are used. For the fibonacci spheres, the center of the concerning
// * point cloud is used as center of the sphere. The point with the largest
// * distance to the center is then used as radius. This class also provides lots
// * of helper methods that make the conversion between different data types
// * simpler.
// */
//public class ShapeFitting {
//	/**
//	 * The minimum length of cylinders, frustums, and cones. This value is also
//	 * the minimum diameter for spheres. This value is set as default size if
//	 * fitted objects are too small or have a length of 0. If an object gets a
//	 * length of 0, it is not displayed as circle, but not displayed instead.
//	 * This is misleading behavior for the user and should be avoided. Because
//	 * all fitting methods are only approximative algorithms, a minimal error
//	 * for vanishing point clouds or objects should be justifiable.
//	 */
//	private static final double MINIMUM_OBJECT_LENGTH = 0.1;
//
//	/**
// 	 * Sets the rotation and the translation of an object (extending Null). This
//	 * can be used for nearly all objects from the de.grogra.imp3d.objects
//	 * package. All other values in the translation matrix are reset by
//	 * executing this method.
//	 */
//	public static void setRotationAndTranslation(Null object,ClusterVector position,ClusterVector direction) {
//		if (direction != null) {
//			double x = direction.get(0);
//			double y = direction.get(1);
//			double z = direction.get(2);
//			double radius = Math.sqrt(x * x + y * y + z * z);
//			double xAngle = 0;
//			double yAngle = Math.acos(z / radius);
//			double zAngle = Math.atan2(y, x);
//			object.setRotation(xAngle, yAngle, zAngle);
//		}
//		Matrix4d result = object.getLocalTransformation();
//		result.m03 = position.get(0);
//		result.m13 = position.get(1);
//		result.m23 = position.get(2);
//		object.setTransform(result);
//	}
//	
//	/**
//	 * Fits a sphere to the given point cloud, so that the sphere encloses all
//	 * points of the point cloud and the volume is as minimal as possible. The
//	 * average parameter can be set to true to fit the point cloud so that a
//	 * regression is executed for all points and the average radius is used. If
//	 * the average parameter is set to false, the maximum radius of all points
//	 * is used.
//	 *
//	 * @param pointCloud The point cloud that should be used
//	 * @param average Must be set to true to use average fitting and to false to
//	 * use maximum fitting
//	 * @param The fitted sphere object
//	 */
//	public static Sphere fitSphereToPointCloud(CloudList pointCloud, boolean average) {
//		
//		ArrayPoint center = pointCloud.getCenter();
//		double radius = 0.0;
//		if (average) {
//			double distanceSum = 0.0;
//			List<Cloud.Point> points = Arrays.asList( pointCloud.getPoints() );
//			int index = 0;
//			while (index < points.size()) {
//				distanceSum += Utils.getDistanceToPoint(((ArrayPoint)points.get(index)), center);
//				index++;
//			}
//			radius = distanceSum / (double)(points.size());
//		} else {
//			ArrayPoint point = pointCloud.getPointWithMostFarDistanceToCenterPosition();
//			radius = Utils.getDistanceToPoint(center, point);
//		}
//		// The minimum length must be divided by 2 to get the radius instead of the diameter
//		radius = Math.max(MINIMUM_OBJECT_LENGTH / 2.0, radius);
//		// rotation is null (because it's a sphere)
//		ClusterVector position = new ClusterVector(center.toFloat());
//		Sphere sphere = new Sphere();
//		setRotationAndTranslation(sphere, position,  null);
//		sphere.setRadius((float)radius);
//		return sphere;
//	}
//	
//	/**
//	 * Fits a cylinder to the given point cloud, so that the cylinder encloses
//	 * all points of the point cloud and the volume is as minimal as possible.
//	 * With the precision parameter, the number of points in the fibonacci
//	 * sphere can be specified. The average parameter can be set to true to fit
//	 * the point cloud so that a regression is executed for all points and the
//	 * average radius is used. If the average parameter is set to false, the
//	 * maximum radius of all points is used.
//	 *
//	 * @param pointCloud The point cloud that should be used
//	 * @param average Must be set to true to use average fitting and to false to
//	 * use maximum fitting
//	 * @param precision The number of points for the fibonacci sphere
//	 * @param The fitted cylinder object
//	 */
//	public static de.grogra.imp3d.objects.Cylinder fitCylinderToPointCloud(CloudList pointCloud, boolean average, int precision) {
//		Cylinder cylinder = fitCylinderWithFibonacciSphereAnalysis(pointCloud, precision);
//		if (average) {
//			List<Cloud.Point> points = Arrays.asList( pointCloud.getPoints() );
//			ClusterVector  position = cylinder.getPosition();
//			ClusterVector  direction = cylinder.getDirection();
//			double distanceSum = 0.0;
//			int index = 0;
//			while (index < points.size()) {
//				distanceSum += Utils.getDistanceToLine(((ArrayPoint)points.get(index)), position, direction);
//				index++;
//			}
//			cylinder.setRadius((float)(distanceSum / (double)(points.size())));
//		}
//		cylinder.setLength((float)Math.max(MINIMUM_OBJECT_LENGTH, cylinder.getLength()));
//		
//		de.grogra.imp3d.objects.Cylinder c = new de.grogra.imp3d.objects.Cylinder();
//		setRotationAndTranslation(c, cylinder.getPosition(), cylinder.getDirection());
//		c.setRadius((float)(cylinder.getRadius()));
//		c.setLength((float)(cylinder.getLength()));
//		return c;
//	}
//	
//	/**
//	 * Fits a cylinder to the given point cloud and returns it. With the
//	 * precision parameter, the number of virtual test points can be specified.
//	 * As higher the number of points, as higher the calculation time and as
//	 * higher the precision. Recommended value is 1000. If the point cloud is
//	 * null or empty, an exception is thrown.
//	 *
//	 * @param pointCloud The point cloud to fit the cylinder to
//	 * @param precision The number of points for the fitting algorithm
//	 * @return The fitting cylinder with all parameters set
//	 * @throws IllegalArgumentException If the point cloud is empty
//	 * @throws NullPointerException If the point cloud is null
//	 */
//	private static Cylinder fitCylinderWithFibonacciSphereAnalysis(CloudList pointCloud, int precision) {
//		if (pointCloud == null) {
//			String message = "null pointcloud";
//			throw new NullPointerException(message);
//		}
//		int number = pointCloud.getNumberOfPoints();
//		if (number == 0) {
//			String message = "empty pointcloud";
//			throw new IllegalArgumentException(message);
//		}
//		ArrayPoint center = pointCloud.getCenter();
//		ArrayPoint mostFarPoint = pointCloud.getPointWithMostFarDistanceToCenterPosition();
//		double radius = Utils.getDistanceToPoint(mostFarPoint, center);
//		int numberOfPoints = precision;
//		List<ArrayPoint> sphere = createFibonacciSphere(center, radius, numberOfPoints);
//		double score = Double.MAX_VALUE;
//		Cylinder bestCylinder = null;
//		int index = 0;
//		while (index < numberOfPoints) {
//			ClusterVector direction = ClusterVector.subtractVectors(new ClusterVector( sphere.get(index).toFloat()), new ClusterVector(center.toFloat()));
//			Cylinder cylinder = fitCylinderToPointCloudWithGivenDirection(pointCloud, direction);
//			//double currentScore = cylinder.getScore(pointCloud);
//			// This is possible because the cylinder is always the
//			// bounding-cylinder for the point cloud and the cylinder with the
//			// smallest volume is the best one.
//			double currentScore = cylinder.getVolume();
//			if (currentScore < score) {
//				score = currentScore;
//				bestCylinder = cylinder;
//			}
//			index++;
//		}
//		return bestCylinder;
//	}
//	/**
//	 * Creates and returns a fibonacci sphere, represented by a list of points.
//	 * With the parameters, the center position, the radius and the number of
//	 * points can be specified.
//	 *
//	 * @param center The center position for the fibonacci sphere
//	 * @param radius The radius for the fibonacci sphere
//	 * @param number The number of points for the fibonacci sphere
//	 * @return The list of points that represend the fibonacci sphere
//	 */
//	private static List<ArrayPoint> createFibonacciSphere(ArrayPoint center, double radius, int number) {
//		List<ArrayPoint> points = new ArrayList<>();
//		double phi = Math.PI * (3.0 - Math.sqrt(5));
//		int index = 0;
//		while (index < number) {
//			double y = radius - ((double)(index) / (double)(number - 1)) * 2.0 * radius;
//			double internalRadius = Math.sqrt(radius * radius - y * y);
//			double theta = phi * (double)(index);
//			double x = Math.cos(theta) * internalRadius;
//			double z = Math.sin(theta) * internalRadius;
//			float[] pos = center.toFloat();
//			points.add(new ArrayPoint(null, new float[] {(float) (pos[0] + x),(float) (pos[1] + y),(float) (pos[2] + z)}));
//			index++;
//		}
//		return points;
//	}
//	
//	/**
//	 * Creates and returns a cylinder that fits to the given point cloud and the
//	 * given direction. The returned cylinder has its center point in the center
//	 * point of the point cloud and the direction vector is the one that was
//	 * given. The length and the radius of the cylinder are adapted, so that all
//	 * points of the point cloud are contained in the cylinder, but the volume
//	 * of the cylinder is as small as possible.
//	 *
//	 * @param pointCloud The point cloud that should be used for the fitting
//	 * @param direction The direction vector that should be used for the fitting
//	 * @return The cylinder with the fitting center point, radius and length
//	 */
//	private static Cylinder fitCylinderToPointCloudWithGivenDirection(CloudList pointCloud, ClusterVector direction) {
//		ArrayPoint center = pointCloud.getCenter();
//		double maximumLength = 0;// distance to plane on center point and direction vector as normal vector
//		double maximumRadius = 0;// distance to normal vector (direction vector)
//		int index = 0;
//		while (index < Arrays.asList(pointCloud.getPoints()).size()) {
//			ArrayPoint point = (ArrayPoint) Arrays.asList(pointCloud.getPoints()).get(index);
//			double distanceToPlane = Utils.getDistanceToPlane(point, new ClusterVector(center.toFloat()), direction);
//			double distanceToNormal = Utils.getDistanceToLine(point, new ClusterVector(center.toFloat()), direction);
//			if (distanceToPlane > maximumLength) {
//				maximumLength = distanceToPlane;
//			}
//			if (distanceToNormal > maximumRadius) {
//				maximumRadius = distanceToNormal;
//			}
//			index++;
//		}
//		ClusterVector length = direction.clone();
//		length.trimToLength(maximumLength);
//				
//		return new Cylinder(ClusterVector.subtractVectors(new ClusterVector(center.toFloat()), length), direction, maximumRadius, 2 * maximumLength);
//	}
//	
//	/**
//	 * Fits a frustum to the given point cloud, so that the frustum encloses all
//	 * points of the point cloud and the volume is as minimal as possible. With
//	 * the precision parameter, the number of points in the fibonacci sphere can
//	 * be specified. The average parameter can be set to true to fit the point
//	 * cloud so that a regression is executed for all points and the average
//	 * radius is used. If the average parameter is set to false, the maximum
//	 * radius of all points is used.
//	 *
//	 * @param pointCloud The point cloud that should be used
//	 * @param average Must be set to true to use average fitting and to false to
//	 * use maximum fitting
//	 * @param precision The number of points for the fibonacci sphere
//	 * @param The fitted frustum object
//	 */
//	public static Frustum fitFrustumToPointCloud(CloudList pointCloud, boolean average, int precision) {
//		Cylinder cylinder = fitCylinderToPointCloud(pointCloud, average, precision);
//		double[] regression = PointCloudFittingTools.calculateLinearRegression(cylinder, pointCloud);
//		double slope = regression[0];
//		double intercept = regression[1];
//		ClusterVector  position = cylinder.getPosition();
//		ClusterVector  direction = cylinder.getDirection();
//		// the baseRadius and topRadius are calculated for the case that all
//		// points should be (in average) on the frustum surface
//		double baseRadius = slope * 0.0 + intercept;
//		double topRadius = slope * cylinder.getLength() + intercept;
//		if (!average) {
//			// If the maximum mode is active, the linear regression is shifted
//			// away from the main axis. The regression is shifted so that the
//			// point with the largest vertical distance to the regression makes
//			// the new regression
//			List<Cloud.Point> points = Arrays.asList(pointCloud.getPoints());
//			double maximumDistance = 0.0;
//			int index = 0;
//			while (index < points.size()) {
//				ArrayPoint point = (ArrayPoint) points.get(index);
//				double distanceToMainAxis = Utils.getDistanceToLine(point, position, direction);
//				double distanceToBasePlane = Utils.getDistanceToPlane(point, position, direction);
//				// The distanceToBasePlane is always between 0 and
//				// cylinder.getLength() because the cylinder was created so that
//				// it contains all points.
//				double distance = distanceToMainAxis - (slope * distanceToBasePlane + intercept);
//				if (distance > maximumDistance) {
//					maximumDistance = distance;
//				}
//				index++;
//			}
//			baseRadius += maximumDistance;
//			topRadius += maximumDistance;
//		}
//		double frustumLength = cylinder.getLength();
//		frustumLength = Math.max(PointCloudFittingTools.MINIMUM_OBJECT_LENGTH, frustumLength);
//		if (topRadius < baseRadius) {
//			return new Frustum(position, direction, baseRadius, topRadius, frustumLength);
//		} else {
//			// flip frustum if top radius is larger than bottom radius
//			ClusterVector  length = direction.clone();
//			length.trimToLength(cylinder.getLength());
//			ClusterVector  newPosition = ClusterVector.addVectors(position, length);
//			ClusterVector  newDirection = ClusterVector.multiplyWithScalar(direction, -1);
//			double newBaseRadius = topRadius;
//			double newTopRadius = baseRadius;
//			return new Frustum(newPosition, newDirection, newBaseRadius, newTopRadius, frustumLength);
//		}
//	}
//
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	/**
//	 * Generates / Fits a cone for each point cloud in the current 3d view
//	 * selection and adds the cones to the 3d view. If the selection does not
//	 * fit, an error dialog is shown. The point clouds can optionally be
//	 * removed.
//	 *
//	 * WARNING: Do not remove this method, even if it is not called by other
//	 * java methods! This method is called by a button in the GroIMP menu (with
//	 * reflection). Otherwise, a MethodNotFoundException will be thrown.
//	 *
//	 * @param item The item that was clicked
//	 * @param information Information about the clicked menu entry
//	 * @param context The context where the button was clicked
//	 */
//	public static void guiFitConesToPointClouds(Item item, Object information, Context context) {
//		List<Object> objects = PointCloudTools.loadSelectedObjects(context);
//		if (!PointCloudTools.checkPointCloudSelection(objects, 1)) {
//			return;
//		}
//		Imp3dCloud[] array;
//		array = PointCloudTools.secureCastPointClouds(objects);
//		PointCloudDialog dialog = PointCloudDialog.getInstance();
//		boolean success = dialog.requestFittingParameters(true);
//		if (!success) {
//			return;
//		}
//		int option = (array.length == 1 ? PointCloudDialog.FIT_ONE : PointCloudDialog.FIT_MULTIPLE);
//		int keep = PointCloudDialog.keepRemoveCancelPopup(option);
//		if (keep == PointCloudDialog.CANCEL) {
//			return;
//		}
//		int precision = dialog.getPrecision();
//		boolean average = dialog.shouldUseAverageFittingOption();
//		de.grogra.imp3d.objects.Cone[] cones;
//		cones = PointCloudTools.fitConesToPointClouds(array, average, precision);
//		PointCloudTools.addNodesToTree(cones, context);
//		if (keep == PointCloudDialog.REMOVE) {
//			PointCloudTools.removeSelectedObjectsFrom3DView(context);
//		}
//	}
//	public static void guiFitMeshToPointClouds(Item item, Object information, Context context) {
//		List<Object> objects = PointCloudTools.loadSelectedObjects(context);
//		if (!PointCloudTools.checkPointCloudSelection(objects, 1)) {
//			return;
//		}
//		Imp3dCloud[] array;
//		array = PointCloudTools.secureCastPointClouds(objects);
//		PointCloudDialog dialog = PointCloudDialog.getInstance();
//		int option = (array.length == 1 ? PointCloudDialog.FIT_ONE : PointCloudDialog.FIT_MULTIPLE);
//
//		int keep = PointCloudDialog.keepRemoveCancelPopup(option);
//		if (keep == PointCloudDialog.CANCEL) {
//			return;
//		}
//		int precision = dialog.getPrecision();
//		boolean average = dialog.shouldUseAverageFittingOption();
//		de.grogra.imp3d.objects.MeshNode[] mesh;
//		mesh = PointCloudTools.fitMeshToPointClouds(array, average, precision);
//		
//		PointCloudTools.addNodesToTree(mesh, context);
//		if (keep == PointCloudDialog.REMOVE) {
//			PointCloudTools.removeSelectedObjectsFrom3DView(context);
//		}
//	}
//	
//	/**
//	 * Generates / Fits an automatic object (sphere, cylinder, frustum or cone)
//	 * for each point cloud in the current 3d view selection and adds the
//	 * objects to the 3d view. If the selection does not fit, an error dialog is
//	 * shown. The point clouds can optionally be removed. The algorithm that
//	 * works in the background of this function returns the object for each
//	 * point cloud with the best score. If multiple point clouds are selected,
//	 * different kinds of objects may be generated.
//	 *
//	 * WARNING: Do not remove this method, even if it is not called by other
//	 * java methods! This method is called by a button in the GroIMP menu (with
//	 * reflection). Otherwise, a MethodNotFoundException will be thrown.
//	 *
//	 * @param item The item that was clicked
//	 * @param information Information about the clicked menu entry
//	 * @param context The context where the button was clicked
//	 */
//	public static void guiFitAutomaticObjectsToPointClouds(Item item, Object information, Context context) {
//		List<Object> objects = PointCloudTools.loadSelectedObjects(context);
//		if (!PointCloudTools.checkPointCloudSelection(objects, 1)) {
//			return;
//		}
//		Imp3dCloud[] array;
//		array = PointCloudTools.secureCastPointClouds(objects);
//		PointCloudDialog dialog = PointCloudDialog.getInstance();
//		boolean success = dialog.requestFittingParameters(true);
//		if (!success) {
//			return;
//		}
//		int option = (array.length == 1 ? PointCloudDialog.FIT_ONE : PointCloudDialog.FIT_MULTIPLE);
//		int keep = PointCloudDialog.keepRemoveCancelPopup(option);
//		if (keep == PointCloudDialog.CANCEL) {
//			return;
//		}
//		int precision = dialog.getPrecision();
//		boolean average = dialog.shouldUseAverageFittingOption();
//		Node[] nodes = PointCloudTools.fitAutomaticObjectsToPointClouds(array, average, precision);
//		PointCloudTools.addNodesToTree(nodes, context);
//		if (keep == PointCloudDialog.REMOVE) {
//			PointCloudTools.removeSelectedObjectsFrom3DView(context);
//		}
//	}
//	
//
//	/**
//	 * Generates / Fits an array of cylinders to the given array of point clouds
//	 * and returns it. This method can be called from XL code and is fully
//	 * compatible to the 3D objects in de.grogra.imp3d.objects. With the
//	 * precision parameter, the number of points in the fibonacci sphere can be
//	 * specified. The average parameter can be set to true to fit the point
//	 * cloud so that a regression is executed for all points and the average
//	 * radius is used. If the average parameter is set to false, the maximum
//	 * radius of all points is used.
//	 *
//	 * WARNING: Do not remove this method, even if it is not used by other
//	 * java methods! This method is made to be called in XL code (with
//	 * reflection). Otherwise, a MethodNotFoundException will be thrown.
//	 *
//	 * @param pointClouds The array of point clouds to fit the cylinders to
//	 * @param average Must be set to true to use average fitting and to false to
//	 * use maximum fitting
//	 * @param precision The number of points for the fibonacci sphere
//	 * @return The cylinders that were generated fitting to the point clouds
//	 */
//	public static de.grogra.imp3d.objects.Cylinder[] fitCylindersToPointClouds(Imp3dCloud[] pointClouds, boolean average, int precision) {
//		de.grogra.imp3d.objects.Cylinder[] cylinders = new de.grogra.imp3d.objects.Cylinder[pointClouds.length];
//		int index = 0;
//		while (index < pointClouds.length) {
//			cylinders[index] = PointCloudTools.fitCylinderToPointCloud(pointClouds[index], average, precision);
//			index++;
//		}
//		return cylinders;
//	}
//	/**
//	 * Generates / Fits a cylinder to the given point cloud and returns it. This
//	 * method can be called from XL code and is fully compatible to the 3D
//	 * objects in de.grogra.imp3d.objects. With the precision parameter, the
//	 * number of points in the fibonacci sphere can be specified. The average
//	 * parameter can be set to true to fit the point cloud so that a regression
//	 * is executed for all points and the average radius is used. If the average
//	 * parameter is set to false, the maximum radius of all points is used.
//	 *
//	 * WARNING: Do not remove this method, even if it is not used by other
//	 * java methods! This method is made to be called in XL code (with
//	 * reflection). Otherwise, a MethodNotFoundException will be thrown.
//	 *
//	 * @param pointCloud The point cloud to fit the cylinder to
//	 * @param average Must be set to true to use average fitting and to false to
//	 * use maximum fitting
//	 * @param precision The number of points for the fibonacci sphere
//	 * @return The cylinder that was generated fitting to the point cloud
//	 */
//	public static de.grogra.imp3d.objects.Cylinder fitCylinderToPointCloud(Imp3dCloud pointCloud, boolean average, int precision) {
//		CloudList internalPointCloud = PointCloudTools.convertPointCloud(pointCloud);
//		Cylinder internalCylinder = PointCloudFittingTools.fitCylinderToPointCloud(internalPointCloud, average, precision);
//		return PointCloudFittingTools.convertCylinder(internalCylinder);
//	}
//	/**
//	 * Generates / Fits an array of frustums to the given array of point clouds
//	 * and returns it. This method can be called from XL code and is fully
//	 * compatible to the 3D objects in de.grogra.imp3d.objects. With the
//	 * precision parameter, the number of points in the fibonacci sphere can be
//	 * specified. The average parameter can be set to true to fit the point
//	 * cloud so that a regression is executed for all points and the average
//	 * radius is used. If the average parameter is set to false, the maximum
//	 * radius of all points is used.
//	 *
//	 * WARNING: Do not remove this method, even if it is not used by other
//	 * java methods! This method is made to be called in XL code (with
//	 * reflection). Otherwise, a MethodNotFoundException will be thrown.
//	 *
//	 * @param pointClouds The array of point clouds to fit the frustums to
//	 * @param average Must be set to true to use average fitting and to false to
//	 * use maximum fitting
//	 * @param precision The number of points for the fibonacci sphere
//	 * @return The frustums that were generated fitting to the point clouds
//	 */
//	public static de.grogra.imp3d.objects.Frustum[] fitFrustumsToPointClouds(Imp3dCloud[] pointClouds, boolean average, int precision) {
//		de.grogra.imp3d.objects.Frustum[] frustums = new de.grogra.imp3d.objects.Frustum[pointClouds.length];
//		int index = 0;
//		while (index < pointClouds.length) {
//			frustums[index] = PointCloudTools.fitFrustumToPointCloud(pointClouds[index], average, precision);
//			index++;
//		}
//		return frustums;
//	}
//	/**
//	 * Generates / Fits a frustum to the given point cloud and returns it. This
//	 * method can be called from XL code and is fully compatible to the 3D
//	 * objects in de.grogra.imp3d.objects. With the precision parameter, the
//	 * number of points in the fibonacci sphere can be specified. The average
//	 * parameter can be set to true to fit the point cloud so that a regression
//	 * is executed for all points and the average radius is used. If the average
//	 * parameter is set to false, the maximum radius of all points is used.
//	 *
//	 * WARNING: Do not remove this method, even if it is not used by other
//	 * java methods! This method is made to be called in XL code (with
//	 * reflection). Otherwise, a MethodNotFoundException will be thrown.
//	 *
//	 * @param pointCloud The point cloud to fit the frustum to
//	 * @param average Must be set to true to use average fitting and to false to
//	 * use maximum fitting
//	 * @param precision The number of points for the fibonacci sphere
//	 * @return The frustum that was generated fitting to the point cloud
//	 */
//	public static de.grogra.imp3d.objects.Frustum fitFrustumToPointCloud(Imp3dCloud pointCloud, boolean average, int precision) {
//		CloudList internalPointCloud = PointCloudTools.convertPointCloud(pointCloud);
//		Frustum internalFrustum = PointCloudFittingTools.fitFrustumToPointCloud(internalPointCloud, average, precision);
//		return PointCloudFittingTools.convertFrustum(internalFrustum);
//	}
//	/**
//	 * Generates / Fits an array of cones to the given array of point clouds and
//	 * returns it. This method can be called from XL code and is fully
//	 * compatible to the 3D objects in de.grogra.imp3d.objects. With the
//	 * precision parameter, the number of points in the fibonacci sphere can be
//	 * specified. The average parameter can be set to true to fit the point
//	 * cloud so that a regression is executed for all points and the average
//	 * radius is used. If the average parameter is set to false, the maximum
//	 * radius of all points is used.
//	 *
//	 * WARNING: Do not remove this method, even if it is not used by other
//	 * java methods! This method is made to be called in XL code (with
//	 * reflection). Otherwise, a MethodNotFoundException will be thrown.
//	 *
//	 * @param pointClouds The array of point clouds to fit the cones to
//	 * @param average Must be set to true to use average fitting and to false to
//	 * use maximum fitting
//	 * @param precision The number of points for the fibonacci sphere
//	 * @return The cones that were generated fitting to the point clouds
//	 */
//	public static de.grogra.imp3d.objects.Cone[] fitConesToPointClouds(Imp3dCloud[] pointClouds, boolean average, int precision) {
//		de.grogra.imp3d.objects.Cone[] cones = new de.grogra.imp3d.objects.Cone[pointClouds.length];
//		int index = 0;
//		while (index < pointClouds.length) {
//			cones[index] = PointCloudTools.fitConeToPointCloud(pointClouds[index], average, precision);
//			index++;
//		}
//		return cones;
//	}
//	public static de.grogra.imp3d.objects.MeshNode[] fitMeshToPointClouds(Imp3dCloud[] pointClouds, boolean average, int precision) {
//		de.grogra.imp3d.objects.MeshNode[] mesh = new de.grogra.imp3d.objects.MeshNode[pointClouds.length];
//		int index = 0;
//		while (index < pointClouds.length) {
//			mesh[index] = PointCloudTools.fitMeshToPointCloud(pointClouds[index], average, precision);
//			index++;
//		}
//		return mesh;
//	}
//	
//	/**
//	 * Generates / Fits a cone to the given point cloud and returns it. This
//	 * method can be called from XL code and is fully compatible to the 3D
//	 * objects in de.grogra.imp3d.objects. With the precision parameter, the
//	 * number of points in the fibonacci sphere can be specified. The average
//	 * parameter can be set to true to fit the point cloud so that a regression
//	 * is executed for all points and the average radius is used. If the average
//	 * parameter is set to false, the maximum radius of all points is used.
//	 *
//	 * WARNING: Do not remove this method, even if it is not used by other
//	 * java methods! This method is made to be called in XL code (with
//	 * reflection). Otherwise, a MethodNotFoundException will be thrown.
//	 *
//	 * @param pointCloud The point cloud to fit the cone to
//	 * @param average Must be set to true to use average fitting and to false to
//	 * use maximum fitting
//	 * @param precision The number of points for the fibonacci sphere
//	 * @return The cone that was generated fitting to the point cloud
//	 */
//	public static de.grogra.imp3d.objects.Cone fitConeToPointCloud(Imp3dCloud pointCloud, boolean average, int precision) {
//		CloudList internalPointCloud = PointCloudTools.convertPointCloud(pointCloud);
//		Cone internalCone = PointCloudFittingTools.fitConeToPointCloud(internalPointCloud, average, precision);
//		return PointCloudFittingTools.convertCone(internalCone);
//	}
//	public static de.grogra.imp3d.objects.MeshNode fitMeshToPointCloud(Imp3dCloud pointCloud, boolean average, int precision) {
//		//PointCloud internalPointCloud = PointCloudTools.convertPointCloud(pointCloud);
//		MeshNode internalMesh = PointCloudFittingTools.fitMeshToPointCloud(pointCloud, average, precision);
//		return PointCloudFittingTools.convertMesh(internalMesh);
//	}
//
//	/**
//	 * Generates / Fits an array of automatically selected objects (spheres,
//	 * cylinders, frustums and cones) to the given array of point clouds and
//	 * returns it. This method can be called from XL code and is fully
//	 * compatible to the 3D objects in de.grogra.imp3d.objects. If this method
//	 * is used to fit multiple point clouds, multiple different types of objects
//	 * can be returned. With the precision parameter, the number of points in
//	 * the fibonacci sphere can be specified. The average parameter can be set
//	 * to true to fit the point cloud so that a regression is executed for all
//	 * points and the average radius is used. If the average parameter is set to
//	 * false, the maximum radius of all points is used.
//	 *
//	 * WARNING: Do not remove this method, even if it is not used by other
//	 * java methods! This method is made to be called in XL code (with
//	 * reflection). Otherwise, a MethodNotFoundException will be thrown.
//	 *
//	 * @param pointClouds The array of point clouds to fit the objects to
//	 * @param average Must be set to true to use average fitting and to false to
//	 * use maximum fitting
//	 * @param precision The number of points for the fibonacci sphere
//	 * @return The objects that were generated fitting to the point clouds
//	 */
//	public static Node[] fitAutomaticObjectsToPointClouds(Imp3dCloud[] pointClouds, boolean average, int precision) {
//		Node[] nodes = new Node[pointClouds.length];
//		int index = 0;
//		while (index < pointClouds.length) {
//			nodes[index] = PointCloudTools.fitAutomaticObjectToPointCloud(pointClouds[index], average, precision);
//			index++;
//		}
//		return nodes;
//	}
//	/**
//	 * Generates / Fits an automatically selected object (sphere, cylinder,
//	 * frustum or cone) to the given point cloud and returns it. This method can
//	 * be called from XL code and is fully compatible to the 3D objects in
//	 * de.grogra.imp3d.objects. With the precision parameter, the number of
//	 * points in the fibonacci sphere can be specified. The average parameter
//	 * can be set to true to fit the point cloud so that a regression is
//	 * executed for all points and the average radius is used. If the average
//	 * parameter is set to false, the maximum radius of all points is used.
//	 *
//	 * WARNING: Do not remove this method, even if it is not used by other
//	 * java methods! This method is made to be called in XL code (with
//	 * reflection). Otherwise, a MethodNotFoundException will be thrown.
//	 *
//	 * @param pointCloud The point cloud to fit the object to
//	 * @param average Must be set to true to use average fitting and to false to
//	 * use maximum fitting
//	 * @param precision The number of points for the fibonacci sphere
//	 * @return The object that was generated fitting to the point cloud
//	 */
//	public static Node fitAutomaticObjectToPointCloud(Imp3dCloud pointCloud, boolean average, int precision) {
//		CloudList internalPointCloud = PointCloudTools.convertPointCloud(pointCloud);
//		Shape3D internalNode = PointCloudFittingTools.fitAutomaticObjectToPointCloud(internalPointCloud, average, precision);
//		return PointCloudFittingTools.convertShape3D(internalNode);
//	}
//}



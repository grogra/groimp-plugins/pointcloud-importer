package de.grogra.pointcloud.tools;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.grogra.imp3d.objects.ArrayPoint;
import de.grogra.imp3d.objects.BoundedCloud;
import de.grogra.imp3d.objects.Cloud;

public class Octree {

	public static final int OCTREE_CHILDREN = 8;
	private Octree[] children;
	private List<Cloud.Point> points;
	
	/**
	 * The minimum x position of this octree
	 */
	private double minimumX;
	/**
	 * The minimum y position of this octree
	 */
	private double minimumY;
	/**
	 * The minimum z position of this octree
	 */
	private double minimumZ;
	/**
	 * The maximum x position of this octree
	 */
	private double maximumX;
	/**
	 * The maximum y position of this octree
	 */
	private double maximumY;
	/**
	 * The maximum z position of this octree
	 */
	private double maximumZ;
	/**
	 * The layer of this octree
	 */
	private int layer;
	/**
	 * A constructor to get an octree object. The layer is defined as specified
	 * in the parameter. The points are declared as an empty list. The children
	 * array is initialized.
	 *
	 * @param layer The layer of this octree
	 */
	public Octree(int layer) {
		this.children = new Octree[OCTREE_CHILDREN];
		this.layer = layer;
		this.points = new ArrayList<Cloud.Point>();
	}
	
	public Octree(BoundedCloud pc, int depth) {
		this(0);
		
		setPoints(Arrays.asList( pc.getPoints() ));
		setMinimumX(pc.getMinimumX());
		setMinimumY(pc.getMinimumY());
		setMinimumZ(pc.getMinimumZ());
		setMaximumX(pc.getMaximumX());
		setMaximumY(pc.getMaximumY());
		setMaximumZ(pc.getMaximumZ());
		movePointsToLayer(depth);
	}
	
	
	public void setPoints(List<Cloud.Point> points) {
		this.points = new ArrayList<Cloud.Point>();
		for (Cloud.Point p : points) {
			this.points.add(new ArrayPoint(p.toFloat()));
		}
	}
	
	private void addPoint(Cloud.Point p) {
		this.points.add(p);
	}
	private List<Cloud.Point> getPoints() {
		return this.points;
	}
	
	/**
	 * Sets one of the eight children (one of the eight octants). The octant
	 * parameter must be in the range of 0 to 7.
	 *
	 * @param octant The octant number (0 - 7)
	 * @param child The child octree that should be added
	 */
	public void setChild(int octant, Octree child) {
		this.children[octant] = child;
	}
	/**
	 * Returns the octree child on the given octant (0 - 7). If the octant was
	 * never declared or set to null, the returned object will also be null.
	 *
	 * @param octant The number of the wanted octant (0 - 7)
	 * @return The octant child element or null
	 */
	public Octree getChild(int octant) {
		return this.children[octant];
	}
	/**
	 * Returns the number of non-null child elements. Because the octree has
	 * always eight places to store child elements, only the used children are
	 * counted.
	 *
	 * @return The number of used child elements
	 */
	public int getNumberOfChildren() {
		int index = 0;
		int number = 0;
		while (index < OCTREE_CHILDREN) {
			if (this.children[index] != null) {
				number++;
			}
			index++;
		}
		return number;
	}
	/**
	 * Moves the points of this octree to the respective octree or octrees on
	 * the given layer. If the layer is lower than the layer of this octree, the
	 * points are collected and moved to the given parent node. If the layer is
	 * higher than the layer of this octree node, the points are distributed to
	 * the fitting child elements.
	 *
	 * @param layer The target layer (where to move the points to)
	 */
	public void movePointsToLayer(int layer) {
		this.distributePointsToChildren(layer);
		this.collectPointsFromChildren(layer);
	}
	/**
	 * Distributes the points of this octree to the child elements until they
	 * have reached the given target layer.
	 *
	 * @param layer The target layer (where to move the points to)
	 */
	private void distributePointsToChildren(int layer) {
		if (this.layer >= layer) {
			return;
		}
		while (points.size() > 0) {
			int octant = this.determineOctantForPoint(this.points.get(0));
			if (this.children[octant] == null) {
				this.createChild(octant);
			}
			this.children[octant].addPoint(points.get(0));
			points.remove(0);
		}
		int index = 0;
		while (index < OCTREE_CHILDREN) {
			if (this.children[index] != null) {
				this.children[index].distributePointsToChildren(layer);
			}
			index++;
		}
	}
	/**
	 * Collects the points of all child elements of this octree and moves them
	 * to this octree.
	 *
	 * @param layer The target layer (where to move the points to)
	 */
	private void collectPointsFromChildren(int layer) {
		int index = 0;
		while (index < OCTREE_CHILDREN) {
			Octree child = this.children[index];
			if (child != null) {
				child.collectPointsFromChildren(layer);
				if (this.layer >= layer) {
					this.points.addAll(child.getPoints());
					this.children[index] = null;
				}
			}
			index++;
		}
	}
	/**
	 * Determines and returns the correct octree child element (octant) for the
	 * given point. The point is categorized by its position and the positions
	 * of the child octrees. If the given point is not contained in this octree
	 * and not in the child octrees, 0 is returned.
	 *
	 * @param point The point to get the octant for
	 * @return The octant number for the point or 0 in case of a point outside
	 * this octree node
	 */
	private int determineOctantForPoint(Cloud.Point point) {
		int octant = 0;
		float[] f = point.toFloat();
		if (f.length<3) { return 0; }
		if (f[0] >= this.getCenterX()) {
			octant += 4;
		}
		if (f[1] >= this.getCenterY()) {
			octant += 2;
		}
		if (f[2] >= this.getCenterZ()) {
			octant += 1;
		}
		return octant;
	}
	/**
	 * Returns the neighbor points for the given point. An other point is a
	 * neighbor point if the distance between both points is less than the given
	 * distance. If there are no neighbor points, an empty list is returned.
	 *
	 * @param point The point to get the neighbor points for
	 * @param distance The maximum distance to identify other points as neighbor
	 * points
	 * @return The list of neighbor points or an empty list in case of no
	 * neighbor points
	 */
	public List<Cloud.Point> getNeighbors(Cloud.Point point, double distance) {
		List<Octree> leaves = new ArrayList<Octree>();
		Octree.findNeighborLeaves(point, distance, this, leaves);
		List<Cloud.Point> neighbors = new ArrayList<Cloud.Point>();
		int index = 0;
		while (index < leaves.size()) {
			Octree tree = leaves.get(index);
			List<Cloud.Point> points = tree.getPoints();
			int pointIndex = 0;
			while (pointIndex < points.size()) {
				Cloud.Point otherPoint = points.get(pointIndex);
				if (point != otherPoint && de.grogra.pointcloud.tools.Utils.getDistanceToPoint(point, otherPoint) < distance) {
					neighbors.add(otherPoint);
				}
				pointIndex++;
			}
			index++;
		}
		return neighbors;
	}
	/**
	 * Fills the given list of leaves (octree nodes) with all neighbored octree
	 * nodes that have at least the given maximum distance to the given point.
	 * This method works recursively, so all neighbored octree nodes are
	 * contained in the given list in the end. This list is used to get all
	 * potential neighbor points of the given point.
	 *
	 * @param point The point to get the neighbored octree leaf nodes for
	 * @param distance The maximum distance between the point and an octree node
	 * to be detected as neighbors
	 * @param tree The octree to search in
	 * @param leaves The list of leaf octree elements. The found elements are
	 * added here.
	 */
	private static void findNeighborLeaves(Cloud.Point point, double distance, Octree tree, List<Octree> leaves) {
		if (!Octree.intersects(point, distance, tree)) {
			return;
		}
		if (tree.getNumberOfChildren() > 0) {
			int index = 0;
			while (index < OCTREE_CHILDREN) {
				if (tree.getChild(index) != null) {
					Octree.findNeighborLeaves(point, distance, tree.getChild(index), leaves);
				}
				index++;
			}
		} else {
			leaves.add(tree);
		}
	}
	/**
	 * Returns true if the given point located is in the given octree child. If
	 * the distance between the point and the child octree is lower than the
	 * given distance parameter, the point is also detected as intersecting
	 * point. The point must have the correct coordinates in x, y and z
	 * direction. Otherwise, false is returned.
	 *
	 * @param point The point to check
	 * @param distance The maximum distance between the octree child and the
	 * point to let the point be an intersecting point
	 * @param child The child octree that must match to the point
	 * @return true If the point is located inside the given octree child or has
	 * at least the given maximum distance to it. Otherwise, false is returned.
	 */
	private static boolean intersects(Cloud.Point point, double distance, Octree child) {
		double minimumX = child.getMinimumX() - distance;
		double maximumX = child.getMaximumX() + distance;
		double minimumY = child.getMinimumY() - distance;
		double maximumY = child.getMaximumY() + distance;
		double minimumZ = child.getMinimumZ() - distance;
		double maximumZ = child.getMaximumZ() + distance;
		float[] f = point.toFloat();
		if (f.length<3) { return false; }
		boolean xIntersects = f[0] >= minimumX && f[0] <= maximumX;
		boolean yIntersects = f[1] >= minimumY && f[1] <= maximumY;
		boolean zIntersects = f[2] >= minimumZ && f[2] <= maximumZ;
		return xIntersects && yIntersects && zIntersects;
	}
	/**
	 * Creates a child octree in the given octant.
	 *
	 * @param octant The octant number
	 */
	private void createChild(int octant) {
		Octree octree = new Octree(this.layer + 1);
		if (octant < 4) {
			// x = lower half (0, 1, 2, 3)
			octree.setMinimumX(this.minimumX);
			octree.setMaximumX(this.getCenterX());
		} else {
			// x = upper half (4, 5, 6, 7)
			octree.setMinimumX(this.getCenterX());
			octree.setMaximumX(this.maximumX);
		}
		if ((octant / 2) % 2 == 0) {
			// y = lower half (0, 1, 4, 5)
			octree.setMinimumY(this.minimumY);
			octree.setMaximumY(this.getCenterY());
		} else {
			// y = upper half (2, 3, 6, 7)
			octree.setMinimumY(this.getCenterY());
			octree.setMaximumY(this.maximumY);
		}
		if (octant % 2 == 0) {
			// z = lower half (0, 2, 4, 6)
			octree.setMinimumZ(this.minimumZ);
			octree.setMaximumZ(this.getCenterZ());
		} else {
			// z = upper half (1, 3, 5, 7)
			octree.setMinimumZ(this.getCenterZ());
			octree.setMaximumZ(this.maximumZ);
		}
		this.children[octant] = octree;
	}

	/**
	 * Returns the center x position of this octree. The center x position is
	 * the middle between the minimum x position and the maximum x position.
	 *
	 * @return The center x position
	 */
	public double getCenterX() {
		return this.minimumX + (this.maximumX - this.minimumX) / 2;
	}
	/**
	 * Returns the center y position of this octree. The center y position is
	 * the middle between the minimum y position and the maximum y position.
	 *
	 * @return The center y position
	 */
	public double getCenterY() {
		return this.minimumY + (this.maximumY - this.minimumY) / 2;
	}
	/**
	 * Returns the center z position of this octree. The center z position is
	 * the middle between the minimum z position and the maximum z position.
	 *
	 * @return The center z position
	 */
	public double getCenterZ() {
		return this.minimumZ + (this.maximumZ - this.minimumZ) / 2;
	}
	/**
	 * Sets the minimum x value of this octree.
	 *
	 * @param minimum The new minimum x value for this octree
	 */
	public void setMinimumX(double minimum) {
		this.minimumX = minimum;
	}
	/**
	 * Returns the minimum x value of this octree.
	 *
	 * @return The minimum x value of this octree
	 */
	public double getMinimumX() {
		return this.minimumX;
	}
	/**
	 * Sets the minimum y value of this octree.
	 *
	 * @param minimum The new minimum y value for this octree
	 */
	public void setMinimumY(double minimum) {
		this.minimumY = minimum;
	}
	/**
	 * Returns the minimum y value of this octree.
	 *
	 * @return The minimum y value of this octree
	 */
	public double getMinimumY() {
		return this.minimumY;
	}
	/**
	 * Sets the minimum z value of this octree.
	 *
	 * @param minimum The new minimum z value for this octree
	 */
	public void setMinimumZ(double minimum) {
		this.minimumZ = minimum;
	}
	/**
	 * Returns the minimum z value of this octree.
	 *
	 * @return The minimum z value of this octree
	 */
	public double getMinimumZ() {
		return this.minimumZ;
	}
	/**
	 * Sets the maximum x value of this octree.
	 *
	 * @param maximum The new maximum x value for this octree
	 */
	public void setMaximumX(double maximum) {
		this.maximumX = maximum;
	}
	/**
	 * Returns the maximum x value of this octree.
	 *
	 * @return The maximum x value of this octree
	 */
	public double getMaximumX() {
		return this.maximumX;
	}
	/**
	 * Sets the maximum y value of this octree.
	 *
	 * @param maximum The new maximum y value for this octree
	 */
	public void setMaximumY(double maximum) {
		this.maximumY = maximum;
	}
	/**
	 * Returns the maximum y value of this octree.
	 *
	 * @return The maximum y value of this octree
	 */
	public double getMaximumY() {
		return this.maximumY;
	}
	/**
	 * Sets the maximum z value of this octree.
	 *
	 * @param maximum The new maximum z value for this octree
	 */
	public void setMaximumZ(double maximum) {
		this.maximumZ = maximum;
	}
	/**
	 * Returns the maximum z value of this octree.
	 *
	 * @return The maximum z value of this octree
	 */
	public double getMaximumZ() {
		return this.maximumZ;
	}
}

package de.grogra.pointcloud.tools;

import javax.vecmath.Vector3f;

import de.grogra.imp3d.objects.Cloud;

public class Utils {
	/**
	 * Returns the distance between p1 and the given point p2. The distance
	 * is calculated with the pythagorean theorem.
	 *
	 * @param point The other point
	 * @return The distance between this point and the given point. Or -1 if one the of two
	 * points do not have a 3d corrdinate
	 */
	static public double getDistanceToPoint(Cloud.Point p1, Cloud.Point p2) {
		float f1[] = p1.toFloat();
		float f2[] = p2.toFloat();
		if (f1.length < 3 || f2.length < 3) { return -1; }
		float x = f2[0] - f1[0];
		float y = f2[1] - f1[1];
		float z = f2[2] - f1[2];
		return Math.sqrt(x * x + y * y + z * z);
	}
	
	/**
	 * Returns the distance between p1 and the given vector v2. The distance
	 * is calculated with the pythagorean theorem.
	 *
	 * @param point The other point
	 * @return The distance between this point and the given point. Or -1 if one the of two
	 * points do not have a 3d corrdinate
	 */
	static public double getDistanceToPoint(Cloud.Point p1,Vector3f v2) {
		float f1[] = p1.toFloat();
		if (f1.length < 3 ) { return -1; }
		float x = v2.x - f1[0];
		float y = v2.y - f1[1];
		float z = v2.z - f1[2];
		return Math.sqrt(x * x + y * y + z * z);
	}
	
	
	/**
	 * Returns the distance between the point p1 and the given line (represented
	 * by a position vector and a direction vector).
	 *
	 * @param position The position vector of the line
	 * @param direction The direction vector of the line
	 * @return The distance between this point and that line
	 */
	static public double getDistanceToLine(Cloud.Point p1, ClusterVector position, ClusterVector direction) {
		ClusterVector vector = ClusterVector.subtractVectors(new ClusterVector(p1.toFloat()), position);
		vector = ClusterVector.getCrossProduct3(vector, direction);
		return vector.getPythagoreanLength() / direction.getPythagoreanLength();
	}
	/**
	 * Returns the distance between point p1 and the given plane (represented
	 * by a position vector and a normal vector).
	 *
	 * @param position The position vector of the plane
	 * @param normal The normal vector of the plane
	 * @return The distance between this point and that plane
	 */
	static public double getDistanceToPlane(Cloud.Point p1, ClusterVector position, ClusterVector normal) {
		ClusterVector vector = ClusterVector.subtractVectors(new ClusterVector(p1.toFloat()), position);
		double scalar = ClusterVector.getScalarProduct(vector, normal);
		return Math.abs(scalar) / normal.getPythagoreanLength();
	}
	/**
	 * Returns the signed distance between point p1 and the given plane
	 * (represented by a position vector and a normal vector). Signed means that
	 * the distance is negative if the point is below the plane and the distance
	 * is positive if the point is above the plane.
	 *
	 * @param position The position vector of the plane
	 * @param normal The normal vector of the plane
	 * @return The signed distance between this point and that plane
	 */
	static public double getSignedDistanceToPlane(Cloud.Point p1, ClusterVector position, ClusterVector normal) {
		ClusterVector vector = ClusterVector.subtractVectors(new ClusterVector(p1.toFloat()), position);
		double scalar = ClusterVector.getScalarProduct(vector, normal);
		return scalar / normal.getPythagoreanLength();
	}
	
	static public void addCoordinates(Cloud.Point p1, Cloud.Point p2) {
		float[] f1 = p1.toFloat();
		float[] f2 = p2.toFloat();
		if (f1.length != f2.length ) { return; }
		for (int i =0; i<f1.length;i++) {
			f1[i]+=f2[i];
		}
	}
	
}

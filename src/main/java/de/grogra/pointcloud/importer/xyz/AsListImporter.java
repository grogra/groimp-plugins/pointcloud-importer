package de.grogra.pointcloud.importer.xyz;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

import de.grogra.graph.Graph;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.ArrayPoint;
import de.grogra.imp3d.objects.Cloud;
import de.grogra.mesh.renderer.handler.FloatArrayHandler;
import de.grogra.pf.boot.Main;
import de.grogra.pf.io.FileSource;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.ObjectSource;
import de.grogra.pf.registry.Item;
import de.grogra.pointcloud.importer.PointCloudFilterBase;
import de.grogra.pointcloud.objects.impl.CloudList;
import de.grogra.pointcloud.objects.impl.PointCloudD;
import de.grogra.util.ProgressMonitor;
import de.grogra.util.Utils;
import de.grogra.vfs.FileSystem;


public class AsListImporter extends PointCloudFilterBase implements ObjectSource {

	public AsListImporter(FilterItem item, FilterSource source) {
		super(item, source);
		setFlavor (item.getOutputFlavor());
	}

	public static class LoaderAsNode extends PointCloudFilterBase implements ObjectSource 
	{
		public LoaderAsNode (FilterItem item, FilterSource source)
		{
			super (item, source);
			setFlavor (IOFlavor.NODE);
		}

		public Object getObject () throws IOException {
			Node pcRoot = null;
			if (source instanceof FileSource) {
				Object file = ((FileSource)source).getFile();
				if (file instanceof File) {
					Cloud c = importAsNode((File)file);
					pcRoot = new PointCloudD(c);
					((PointCloudD)pcRoot).setDisplayHandler(new FloatArrayHandler());
				} else if (file != null ) {
					Cloud c = importAsNode(source.getRegistry().getFileSystem() , file );
					pcRoot = new PointCloudD(c);
					((PointCloudD)pcRoot).setDisplayHandler(new FloatArrayHandler());
				}
			}
			
			setProgress("done", ProgressMonitor.DONE_PROGRESS);
			
			return pcRoot;				
		}

		public Node getPoint(long id) {
			return null;
		}

		public Node[] getPoints(long[] list) {
			return null;
		}

		public Node[] getPoints(int[] list) {
			return null;
		}

		public Node[] getPoints(Object list) {
			return null;
		}


		@Override
		public void endExport() {
			allPoints.clear();
		}

		@Override
		public String getKey(Object o) {
			return null;
		}
	}

	private static int estimateNextLine(ByteBuffer buf) {
		int i = buf.limit()-1;
		int j = 0;
		while (i>0) {
			if(buf.get(i) == '\n') {
				return j;
			}
			j++;
			i--;
		}
		return 0;
	}

	static private int getOptionMaxPoint() {
		Item ite = Item.resolveItem(Main.getRegistry(), "/io/plypointcloud");
		return (int) Utils.get(ite, "maxpointpernode", 1000);
	}

	static private boolean getOptionVisible() {
		Item ite = Item.resolveItem(Main.getRegistry(), "/io/plypointcloud");
		return (boolean) Utils.get(ite, "visible", true);
	}

	static class AddLeavesThread extends Thread {

		List<ArrayPoint> result = new ArrayList<ArrayPoint>();
		private ByteBuffer buffer;
		Cloud cloud;

		public AddLeavesThread(ByteBuffer buffer, Cloud c) {
			this.buffer = buffer;
			this.cloud = c;
		}

		@Override
		public void run() {
			float[] pos = new float[3];
			int j = 0; // j goes from 0 to 2 for x, y, z
			StringBuffer number = new StringBuffer();
			boolean commentLine = false;
			boolean numberComplete = false; // true when a non number char is read (i.e. the buffer contains a number)
			boolean lineComplete = false; // true after 3 number read on a line. The other char are additional non supported info

			while (buffer.hasRemaining()) {					
				char c = (char) buffer.get();

				if (commentLine) {
					if (c == '\n') { commentLine= false; }
					else { continue; }
				}
				if (c == '#') { commentLine = true; continue; }
				if (!(c == ' ' || c == ',' || c == ';' || c == '\t' || c == '\r' || c == '\n')) {
					number.append(c);
				} else {
					numberComplete = true;
				}
				if (numberComplete && ! lineComplete) {
					numberComplete = false;
					String val = number.toString();
					if (!val.isEmpty()) {
						number.setLength(0);
						pos[j%3] = Float.valueOf(val);
						j++;
					}
					if (j == 3) {
						j=0;
						lineComplete = true;
						result.add(new ArrayPoint(pos));
						pos = new float[3];
					}
				}
				if (lineComplete) {
					if (c == '\n') { 
						lineComplete= false;
						numberComplete = false;
						number.setLength(0);
					}
				}
			}
		}

		public List<ArrayPoint> getResult(){
			return result;
		}
		public void clear() {
			result.clear();
		}
	}

	/**
	 * Static creation of a Cloud from a SourceFile 
	 */
	public static Cloud importAsNode(File in) throws IOException {
		FileChannel fileChannel = FileChannel.open(in.toPath(), StandardOpenOption.READ);

		Cloud outputCloud = new CloudList();

		int numberOfThreads = Runtime.getRuntime().availableProcessors() -2 ;
		long chunkSize = fileChannel.size() / numberOfThreads;
		if (chunkSize < 1000) {
			numberOfThreads = 0;
			chunkSize = fileChannel.size();
		}
		List<AddLeavesThread> threads = new ArrayList<>();
		long start = 0;
		long stop;
		long missing = fileChannel.size();
		MappedByteBuffer buffer;
		for (int i = 0; i < numberOfThreads; i++) {
			if (missing > chunkSize) {
				buffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, start, chunkSize);
				stop = chunkSize - estimateNextLine(buffer);
			} else {
				stop = missing;
			}
			buffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, start, stop);
			missing -= stop;
			stop += start;
			start = stop;

			AddLeavesThread counterThread = new AddLeavesThread(buffer, outputCloud);
			threads.add(counterThread);
			counterThread.start();

		}
		// last chunk
		buffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, start, missing);
		AddLeavesThread counterThread = new AddLeavesThread(buffer, outputCloud);
		threads.add(counterThread);
		counterThread.start();

		try {
			for (AddLeavesThread thread : threads) {
				thread.join();
				for (ArrayPoint p : thread.getResult()) {
					outputCloud.addPoint(p);
				}
				thread.clear();
			}
		} catch(InterruptedException e) {
			e.printStackTrace();
		}
		return outputCloud;
	}

	/**
	 * Static import of a Virtual file in as a Cloud
	 */
	public static Cloud importAsNode(FileSystem fs, Object in) throws IOException {
				
		InputStream is = fs.getInputStream(in);
		long filesize = fs.getSize(in);
		Cloud outputCloud = new CloudList();

		int numberOfThreads = Runtime.getRuntime().availableProcessors() -2 ;
		long chunkSize = filesize / numberOfThreads;
		if (chunkSize < 1000) {
			numberOfThreads = 0;
			chunkSize = filesize;
		}
		List<AddLeavesThread> threads = new ArrayList<>();

		long start = 0;
		long stop;
		long missing = filesize;
		int w=0;
		ByteBuffer buffer;
		byte[] b1 = new byte[(int)chunkSize];
		for (int i = 0; i < numberOfThreads; i++) {
			if (missing > chunkSize) {
				is.read(b1, w, (int) chunkSize - w);
				buffer = ByteBuffer.wrap(b1);
				stop = chunkSize - estimateNextLine(buffer);
			} else {
				stop = missing;
			}
			w = (int) (chunkSize - stop);
			byte[] b2 = new byte[(int)stop]; 
			System.arraycopy(b1, 0, b2, 0, (int) stop);
			System.arraycopy(b1, (int)stop, b1, 0, w);
			buffer = ByteBuffer.wrap(b2);
			missing -= stop;
			stop += start;
			start = stop;

			AddLeavesThread counterThread = new AddLeavesThread(buffer, outputCloud);
			
			threads.add(counterThread);
			counterThread.start();
		}
		// last chunk
		byte[] b2 = new byte[(int)missing+w];
		System.arraycopy(b1, 0, b2, 0, w);
		is.read(b2, w, (int)missing-w);
		buffer = ByteBuffer.wrap(b2);
		AddLeavesThread counterThread = new AddLeavesThread(buffer, outputCloud);
		threads.add(counterThread);
		counterThread.start();

		try {
			for (AddLeavesThread thread : threads) {
				thread.join();
				for (ArrayPoint p : thread.getResult()) {
					outputCloud.addPoint(p);
				}
				thread.clear();
			}
		} catch(InterruptedException e) {
			e.printStackTrace();
		}
		return outputCloud;
	}

	@Override
	public Object getObject() throws IOException {
		if (source instanceof FileSource) {
			Object file = ((FileSource)source).getFile();
			if (file instanceof File) {
				Cloud c = importAsNode((File)file);
				return c;
			} else if (source instanceof InputStream) {
			
			
			} else if (file != null ) {
				Cloud c = importAsNode(source.getRegistry().getFileSystem() , file );
				return c;
			}
		}
		return null;	
	}

	@Override
	public void endExport() {
		allPoints.clear();
	}
	@Override public Node getPoint(long id) { return null; }
	@Override public Node[] getPoints(Object list) { return null; }
	@Override public String getKey(Object o) { return null; }


}

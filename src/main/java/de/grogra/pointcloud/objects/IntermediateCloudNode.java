package de.grogra.pointcloud.objects;

import de.grogra.graph.impl.Node;

/**
 * An intermediate node in a point cloud graph. Used to reduce the amount of edges on 
 * one single Node.
 */
public abstract class IntermediateCloudNode extends Node {

	/*
	 * max id of its children point. see @{PointCloudPoint#idImport}
	 * Used for balancing and querying
	 */
	protected long maxId;
	
	/*
	 * min id
	 */
	protected long minId;
	
	/*
	 * Number of children that are PointCloudPoints regardless of distance ? recursive?
	 */
	protected long numberChildren;
	
	/*
	 * Level in the tree (height?) depth?
	 */
	protected long level;
	
	public long getMaxId() {
		return maxId;
	}
	public long getMinId() {
		return minId;
	}
	public long getNumberChildren() {
		return numberChildren;
	}
	public long getLevel() {
		return level;
	}
	
	// throw an error if id is already in? Should be managed by persistence manager
	public void addChild(long id) {
		numberChildren++;
		if(minId<0 || minId>id) {
			setMinId(id); 
		}
		if(maxId<0 || maxId<id) {
			setMaxId(id); 
		}
	}
	
	public void setMinId(long id) {
		minId=id;
		if (level>0) {
			Node n = getAxisParent();
			if(n instanceof IntermediateCloudNode) {
				((IntermediateCloudNode)n).setMinId(id);
			}
		}
	}
	public void setMaxId(long id) {
		maxId=id;
		if (level>0) {
			Node n = getAxisParent();
			if(n instanceof IntermediateCloudNode) {
				((IntermediateCloudNode)n).setMaxId(id);
			}
		}
	}

}

/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package de.grogra.pointcloud.objects.impl;
import java.util.ArrayList;
import java.util.List;

import javax.vecmath.Vector3f;

import de.grogra.graph.impl.Node;
import de.grogra.imp.registry.EmbeddedFileObject;
import de.grogra.imp3d.objects.ArrayPoint;
import de.grogra.imp3d.objects.Cloud;
import de.grogra.imp3d.objects.CloudArray;
import de.grogra.mesh.renderer.handler.CollectionDisplayable;
import de.grogra.mesh.renderer.handler.FloatArrayHandler;
import de.grogra.persistence.SCOType;
import de.grogra.util.MimeType;
import de.grogra.xl.lang.Aggregate;

/**
 * An extension of a Cloud array to view point as list.
 * It also implements some methods for applying clustering tools.
 */
public class CloudList extends CloudArray {
	
	//enh:sco SCOType
	
	/**
	 * The points of this point cloud. All points have the id of this point
	 * cloud.
	 */
	transient private List<ArrayPoint> points;
	// enh:field setmethod=setListPoints getmethod=getListPoints
	
	/**
	 * A constructor to get an empty point cloud. The point cloud will have a
	 * default color and an empty list of points.
	 */
	public CloudList() {
		points = new ArrayList<ArrayPoint>();
	}
	public CloudList(Node n) {
		this(n, new ArrayList<ArrayPoint>());
	}
	public CloudList(Cloud apc) {
		this(apc.getNode(), new ArrayList<ArrayPoint>());
		setPoints( apc.getPoints() );
	}
	/**
	 * A constructor to get a point cloud with a list of points. The color will
	 * be a default color.
	 *
	 * @param points The list of points for the point cloud
	 */
	public CloudList(Node parent, List<ArrayPoint> points) {
		super(parent.getName());
		this.cloudNode = parent;
		for (ArrayPoint p : points) {
			addPoint(p);
		}
	}

	public CloudList(float[] f) {
		points = new ArrayList<ArrayPoint>();
		for (Point p : floatsToPoint(f)) {
			addPoint(p);
		}
	}
	
	@Override
	public void setNode(Node cnode) {
		if (cloudNode != cnode) {
//			if ((EmbeddedFileObject)getProvider(false) != null){
//				((EmbeddedFileObject)getProvider(false)).changeNode(cnode);
//			}
		}
		if (cnode !=null && cnode instanceof CollectionDisplayable) {
			((CollectionDisplayable)cnode).setDisplayHandler(new FloatArrayHandler());
			((CollectionDisplayable)cnode).getDisplayHandler() .setNode(cnode);
		}
		this.cloudNode = cnode;
	}

	
	@Override
	public boolean editable() {
		return true;
	}

	@Override
	public Cloud createNew() {
		return new CloudList();
	}

	/**
	 * Returns the number of points in this point cloud.
	 *
	 * @return The number of points in this point cloud
	 */
	public int getNumberOfPoints() {
		return (points == null)? 0 : this.points.size();
	}
	/**
	 * Adds a point to this point cloud. The point cloud reference of the point
	 * is replaced with this point cloud.
	 *
	 * @param point The point to add
	 */
	public void addPoint(ArrayPoint point) {
		if (point.getCloud()!= null) {
			if( point.getCloud() == this) { return; }
			point.getCloud().remove(point);
		}
		if (this.points==null) { this.points = new ArrayList<ArrayPoint>(); }
		this.points.add(point);
		point.setCloud(this);
	}


	/**
	 * Returns the list of points of this point cloud.
	 *
	 * @return The list of points in this point cloud
	 */
	public Point[] getPoints() {
		return (points == null)? new ArrayPoint[0] : points.toArray(new ArrayPoint[0]);
	}

	/**
	 * Returns the center position of this point cloud as a point object. This
	 * method does not return the point with the most-centric position, it
	 * returns a position with the average position of all points in the point
	 * cloud instead.
	 *
	 * @return The center position of this point cloud as a point object
	 */
	public Vector3f getCenter() {
		float centerX = this.getCenterX();
		float centerY = this.getCenterY();
		float centerZ = this.getCenterZ();
		return new Vector3f(centerX, centerY, centerZ);
	}
	/**
	 * Returns the center x position of this point cloud.
	 *
	 * @return The center x position of this point cloud
	 */
	public float getCenterX() {
		return this.getMinimumX() + (this.getMaximumX() - this.getMinimumX()) / 2;
	}
	/**
	 * Returns the center y position of this point cloud.
	 *
	 * @return The center y position of this point cloud
	 */
	public float getCenterY() {
		return this.getMinimumY() + (this.getMaximumY() - this.getMinimumY()) / 2;
	}
	/**
	 * Returns the center z position of this point cloud.
	 *
	 * @return The center z position of this point cloud
	 */
	public float getCenterZ() {
		return this.getMinimumZ() + (this.getMaximumZ() - this.getMinimumZ()) / 2;
	}
	/**
	 * Returns the minimum x position of this point cloud.
	 *
	 * @return The minimum x position of this point cloud
	 */
	public float getMinimumX() {
		return this.getExtremum('x', '-');
	}
	/**
	 * Returns the maximum x position of this point cloud.
	 *
	 * @return The maximum x position of this point cloud
	 */
	public float getMaximumX() {
		return this.getExtremum('x', '+');
	}
	/**
	 * Returns the minimum y position of this point cloud.
	 *
	 * @return The minimum y position of this point cloud
	 */
	public float getMinimumY() {
		return this.getExtremum('y', '-');
	}
	/**
	 * Returns the maximum y position of this point cloud.
	 *
	 * @return The maximum y position of this point cloud
	 */
	public float getMaximumY() {
		return this.getExtremum('y', '+');
	}
	/**
	 * Returns the minimum z position of this point cloud.
	 *
	 * @return The minimum z position of this point cloud
	 */
	public float getMinimumZ() {
		return this.getExtremum('z', '-');
	}
	/**
	 * Returns the maximum z position of this point cloud.
	 *
	 * @return The maximum z position of this point cloud
	 */
	public float getMaximumZ() {
		return this.getExtremum('z', '+');
	}
	/**
	 * Returns the extremum value of this point cloud. The requested extremum
	 * value is specified with the given dimension and the given operator. To
	 * get a minimum value, the operator must be '-'. To get a maximum value,
	 * the operator must be '+'. To get a minimum in x, y or z direction, the
	 * dimension parameter must be 'x', 'y' or 'z'.
	 *
	 * @param dimension Must be 'x', 'y' or 'z' to get the correct dimension
	 * @param operator Must be '-' to get a minimum or '+' to get a maximum
	 * @return The minimum or maximum value in the requested dimension
	 */
	public float getExtremum(char dimension, char operator) {
		if (this.points == null || this.points.size() == 0) {
			return 0;
		}
		boolean minimum = operator == '-';
		boolean maximum = operator == '+';
		float extremum = this.getValue(0, dimension);
		float value;
		int length = this.points.size();
		int index = 1;
		while (index < length) {
			value = this.getValue(index, dimension);
			if ((minimum && value < extremum) || (maximum && value > extremum)) {
				extremum = value;
			}
			index++;
		}
		return extremum;
	}
	/**
	 * Returns the x, y or z coordinate at the given index. The dimension
	 * parameter must be 'x', 'y' or 'z' to get the x, y or z coordinate. If the
	 * dimension parameter is wrong or the index is outside the point list, an
	 * exception is thrown.
	 *
	 * @param index The index of the returned point
	 * @param dimension The dimension (x, y, or z) of the returned point
	 * @return The dimension value of the requested point
	 * @throws IllegalArgumentException If the index is < 0, too high or the
	 * dimension parameter is invalid
	 */
	public float getValue(int index, char dimension) {
		float[] f = this.points.get(index).toFloat();
		if (f.length<3) { return -1; }
		if (dimension == 'x') {
			return f[0];
		} else if (dimension == 'y') {
			return f[1];
		} else if (dimension == 'z') {
			return f[2];
		} else {
			//			String message = Resources.translate("parameterError");
			//			throw new IllegalArgumentException(message);
			throw new IllegalArgumentException("Wrong parameter");
		}
	}

	/**
	 * Sets the given point cloud as point cloud of all given points.
	 *
	 * @param pointCloud The point cloud to set in all points
	 * @param points The list of points where to set the point cloud
	 */
	public static void setPointCloudOfPoints(CloudList pointCloud, List<ArrayPoint> points) {
		int index = 0;
		while (index < points.size()) {
			if (pointCloud != null) {
				points.get(index).setCloud(pointCloud);
			} else {
				points.get(index).setCloud(null);
			}
			index++;
		}
	}
	@Override
	public boolean isEmpty() {
		return points.isEmpty();
	}
	@Override
	public void setPoints(Point[] points) {
		this.points = new ArrayList<ArrayPoint>();
		addPoints(points);
	}
	@Override
	public void setPoints(float[] points) {
		if (points == null) { return; }
		this.points = new ArrayList<ArrayPoint>();
		addPoints(floatsToPoint(points));
	}
	@Override
	public void addPoints(Point[] points) {
		if (points == null) { 
			this.points = new ArrayList<ArrayPoint>(); 
			return;
		}
		for (Point p : points) {
			addPoint(p);
		}
	}
	@Override
	public void addPoint(Point p) {
		if (p == null) { return; }
		if (p.getCloud()!= null) {
			if( p.getCloud() == this) { return; }
			p.getCloud().remove(p);
		}
		if (points == null) { this.points = new ArrayList<ArrayPoint>(); }
		if (p instanceof ArrayPoint) {
			p.setCloud(this);
			this.points.add((ArrayPoint)p);
		} else {
			float[] f = p.toFloat();
			addPoints( floatsToPoint(f));
		}
	}

	@Override
	public Point remove(Object i) {
		if (!(i instanceof Cloud.Point)) {
			throw new RuntimeException("Cannot remove a non Cloud.Point object.");
		}
		if (!((Cloud.Point)i).getCloud().equals(this)) {
			throw new RuntimeException("Point do not belong to this cloud");
		}
		if (points.remove(i)) {
			((Point) i).setCloud(null);
			if (points.isEmpty()) { dispose(); }
			return (Point) i;
		}
		throw new RuntimeException("Point not found");
	}

	@Override
	public Point[] remove(Object[] i) {
		List<Point> tmp = new ArrayList<Point>();
		for (Object o : i) {
			if (o instanceof Point && ((Point)o).getCloud()==this){
				tmp.add( remove(o));
			}
		}
		return tmp.toArray(new Point[0]);
	}
	@Override
	public void removeAll() {
		//		if (points !=null) { 
		//			points.stream().forEach(p->p.setCloud(null));
		//		}
		this.points = new ArrayList<ArrayPoint>();
	}
	
	public void setListPoints(List<ArrayPoint> points) {
		this.points = new ArrayList<ArrayPoint>();
		for (Point p : points) {
			addPoint(p);
		}
	}
	public List<ArrayPoint> getListPoints(){
		return this.points;
	}

	@Override
	public Point peek() {
		return points.get(points.size()-1);
	}
	@Override
	public Point pop() {
		Point p = points.get(points.size()-1);
		points.remove(points.size()-1);
		return p;
	}
	@Override
	public void push(Point p) {
		addPoint(p);
	}
	/**
	 * Parallel version
	 */
	@Override
	public float[] pointsToFloat() {
		if (!points.isEmpty()) {
			int dimension = points.get(0).toFloat().length; //  divisible by 3 
			float[] res = new float[points.size() * dimension];
			int size = points.size() ;
			int numberOfThreads = Runtime.getRuntime().availableProcessors() -2 ;
			List<LoadFloatFromPointThread> threads = new ArrayList<>();
			int chunkSize = size / numberOfThreads;

			int start=0;
			int stop;

			for (int i=0; i<numberOfThreads; i++) {
				if (start < points.size()) {
					stop = start + chunkSize;
					size-= chunkSize;
					LoadFloatFromPointThread t = new LoadFloatFromPointThread(points, res, start, stop);
					threads.add(t);
					t.start();
					start = stop;
				}
			}
			if (size > 0) {
				LoadFloatFromPointThread t = new LoadFloatFromPointThread(points, res, start, start + size);
				threads.add(t);
				t.start();
			}

			try {
				for (LoadFloatFromPointThread thread : threads) {
					thread.join();
				}
			} catch(InterruptedException e) {
				e.printStackTrace();
			}
			return res;
		} else {
			return new float[0];
		}
	}


	@Override
	public Point[] floatsToPoint(float[] f) {
		if (f == null) { return new ArrayPoint[0];}
		if (f.length % 3 != 0) { return new ArrayPoint[0]; }
		ArrayPoint[] res = new ArrayPoint[f.length/3];
		int i = 0,j = 0;
		float[] point = new float[3];
		for (float p : f) {
			if (i == 2) {
				point[i] = p;
				res[j] = new ArrayPoint(point);
				j++;
				i=0;
				point = new float[3];
			} else {
				point[i] = p;
				i++;
			}
		}
		return res;
	}


	static class LoadFloatFromPointThread extends Thread {

		float[] result;
		int start;
		int stop;
		List<ArrayPoint> points;

		public LoadFloatFromPointThread(List<ArrayPoint> points, float[] f, int s, int st) {
			result = f;
			start = s;
			stop = st;
			this.points=points;
		}
		
		//TODO: only works with 3 dimensions points
		@Override
		public void run() {
			int j = 0;
			for (int i = start; i<stop; i++) {
				Point p = points.get(i);
				for (float f : p.toFloat()) {
					result[3*i+j] = f;
					j++;
					if (j==3) {
						j=0;
					}
				}
			}
		}
	}
	
	@Override
	public MimeType getMimeType() {
		return new MimeType("model/x-grogra-cloud-list+xyz");
	}
	@Override
	public boolean hasContent() {
		if (getProvider(false) == null && !(points.size() > 0)) {
			return false;
		}
		return true;
	}
	
	
	public static CloudList create(Aggregate a, Cloud.Point p) {
		if (a.initialize ())
		{
			a.aval1 = new ArrayList<Cloud.Point> ();
		}
		if (a.isFinished ())
		{		
			a.aval = new CloudList();
			((CloudList)a.aval).setPoints( ((ArrayList<Cloud.Point>)a.aval1).toArray(new Cloud.Point[0]) );
		}
		else
		{
			((ArrayList<Cloud.Point>) a.aval1).add (p);
		}
		return null;
	}
	
	@Override
	public Node produceNode() {
		return ( getNode()!=null)? getNode() : new PointCloudD(this);
	}
	
	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final Type $TYPE;

	public static final Type.Field points$FIELD;

	public static class Type extends SCOType
	{
		public Type (Class c, de.grogra.persistence.SCOType supertype)
		{
			super (c, supertype);
		}

		public Type (CloudList representative, de.grogra.persistence.SCOType supertype)
		{
			super (representative, supertype);
		}

		Type (Class c)
		{
			super (c, SCOType.$TYPE);
		}

		private static final int SUPER_FIELD_COUNT = SCOType.FIELD_COUNT;
		protected static final int FIELD_COUNT = SCOType.FIELD_COUNT + 1;

		static Field _addManagedField (Type t, String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			return t.addManagedField (name, modifiers, type, componentType, id);
		}

		@Override
		protected void setObject (Object o, int id, Object value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					((CloudList) o).setListPoints ((List<ArrayPoint>) value);
					return;
			}
			super.setObject (o, id, value);
		}

		@Override
		protected Object getObject (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					return ((CloudList) o).getListPoints ();
			}
			return super.getObject (o, id);
		}

		@Override
		public Object newInstance ()
		{
			return new CloudList ();
		}

	}

	public de.grogra.persistence.ManageableType getManageableType ()
	{
		return $TYPE;
	}


	static
	{
		$TYPE = new Type (CloudList.class);
		points$FIELD = Type._addManagedField ($TYPE, "points", Type.Field.TRANSIENT | Type.Field.PRIVATE  | Type.Field.SCO, de.grogra.reflect.ClassAdapter.wrap (List.class), null, Type.SUPER_FIELD_COUNT + 0);
		$TYPE.validate ();
	}

//enh:end
}

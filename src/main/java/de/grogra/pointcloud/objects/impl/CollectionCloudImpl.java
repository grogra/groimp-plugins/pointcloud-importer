package de.grogra.pointcloud.objects.impl;

import de.grogra.graph.EdgePatternImpl;
import de.grogra.graph.Graph;
import de.grogra.graph.impl.Edge;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.PointCloud;
import de.grogra.pointcloud.objects.CloudGraphBase;
import de.grogra.pointcloud.objects.CollectionCloud;
import de.grogra.pointcloud.objects.IntermediateCloudNode;
import de.grogra.pointcloud.objects.PointCloudLeaf;
import de.grogra.pointcloud.utils.Visitors.GetPointCloudVisitor;

/**
 */
public class CollectionCloudImpl extends Node implements CollectionCloud {
	
	private static final long serialVersionUID = 1L;

	public PointCloud getFirstCloud() {
		Edge e = getFirstEdge();
		while( !(e instanceof PointCloud) && e!=null) {
			e = e.getNext(e.getTarget());
			if ( !(e instanceof PointCloud) && 
					!(e instanceof IntermediateCloudNode) && 
					!(e instanceof PointCloudLeaf)) {
				return null;
			}
		}
		return (PointCloud) e;
	}
	
	@Override
	public PointCloud[] getClouds() {
		GetPointCloudVisitor v = new GetPointCloudVisitor();
		v.init(getCurrentGraphState(),  new EdgePatternImpl
				(Graph.BRANCH_EDGE | Graph.SUCCESSOR_EDGE, Graph.REFINEMENT_EDGE, true, true));
		getGraph().accept(this, v, null);
		return v.bases.toArray(new PointCloud[0]);
	}
	
	
	//enh:insert 
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;


	static
	{
		$TYPE = new NType (new CollectionCloudImpl ());
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new CollectionCloudImpl ();
	}

//enh:end
}

package de.grogra.pointcloud.objects.impl;

import java.util.Arrays;

import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;

import de.grogra.graph.Graph;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.Attributes;
import de.grogra.imp3d.objects.Axis;
import de.grogra.imp3d.objects.AxisBase;
import de.grogra.imp3d.objects.Line;
import de.grogra.imp3d.objects.Null;
import de.grogra.math.TMatrix4d;
import de.grogra.persistence.ManageableType.Field;
import de.grogra.pointcloud.importer.PointCloudFilterBase;
import de.grogra.pointcloud.objects.PointCloudLeaf;
import de.grogra.util.StringMap;
import de.grogra.vecmath.Math2;

public class LeafLineImpl extends Line implements PointCloudLeaf {

	protected long idImport;
	//enh:field setter getter
	
	public enum Keys{
		/**
		 * Required Vertex 1 and 2 ids
		 */
		VERTEX1,
		VERTEX2,
		/**
		 * Optional colored arguments
		 */
		RED,
		GREEN,
		BLUE,
		ALPHA
	}
	
	public LeafLineImpl ()
	{
		super ();
		setLayer (4);
	}

	public LeafLineImpl(StringMap args) {
		this (getNodesFromArgs(args));
		Float[] colors = getColorFromArgs(args);
		if (colors.length==3 && !(Arrays.asList(colors).contains(-1f))) {
			setColor(colors[0], colors[1], colors[2]);
		}
		removeRequiredArgs(args);
		for (String k : args.getKeys()) {
			Field f = this.getNType().getManagedField(k);
			if ( f!=null) {
				f.set (this, new int[] {f.getFieldId()}, args.get(k), null);
			}
		}
	}
	
	public LeafLineImpl(Node p1, Node p2) {
		this ((LeafPointImpl)p1, (LeafPointImpl)p2);	
	}
	
	public LeafLineImpl(Node[] nodes) {
		this ((LeafPointImpl)nodes[0], (LeafPointImpl)nodes[1]);	
	}

	public LeafLineImpl (float x, float y, float z, float dx, float dy, float dz)
	{
		super (x, y, z, dx, dy, dz);
	}
	
	public LeafLineImpl(LeafPointImpl p1, LeafPointImpl p2) {
		super ((float) p1.getTranslation().x, (float)p1.getTranslation().y, (float)p1.getTranslation().z, 
				(float)p2.getTranslation().x-(float) p1.getTranslation().x, 
				(float)p2.getTranslation().y-(float) p1.getTranslation().y, 
				(float) p2.getTranslation().z-(float) p1.getTranslation().z);
		this.addEdgeBitsTo(p1, Graph.REFINEMENT_EDGE, null);
		this.addEdgeBitsTo(p2, Graph.REFINEMENT_EDGE, null);
	}

	
	private static Node[] getNodesFromArgs(StringMap args) {
		PointCloudFilterBase filter = (PointCloudFilterBase) args.get("ctx");
		return new Node[] {filter.getPoint( args.getInt(filter.getKey(Keys.VERTEX1))), 
				filter.getPoint(args.getInt(filter.getKey(Keys.VERTEX2)))  };
	}
	
	private static Float[] getColorFromArgs(StringMap args) {
		PointCloudFilterBase filter = (PointCloudFilterBase) args.get("ctx");
		return new Float[] {args.getFloat(filter.getKey(Keys.RED), -1f), 
			args.getFloat( filter.getKey(Keys.GREEN), -1f), 
			args.getFloat( filter.getKey(Keys.BLUE), -1f) };
	}
	
	/**
	 * Delete args values that are linked with "keys" key
	 */
	private void removeRequiredArgs(StringMap args) {
		PointCloudFilterBase filter = (PointCloudFilterBase) args.get("ctx");
		for (Keys k : Keys.values()) {
			args.remove( filter.getKey(k));
		}
	}
	
	@Override
	public Node getNode() {
		return this;
	}
	
	private static void initType ()
	{
		$TYPE.addIdentityAccessor (Attributes.SHAPE);
	}
	
	/**
	 * Apply a transformation to "node" to make it translate a start position of line
	 * @param node
	 * @return
	 */
	public Node toNull(Null node) {
		if (!(node instanceof Null)) {
			return null;
		}
		((Null)node).setTransform(getTranslation().x, getTranslation().y, getTranslation().z);
		return (Null)node;
	}
	
	/**
	 * Apply a transformation to "node" to make it translate a start position of line.
	 * And rotate toward the end point. 
	 * If the node is of type Axis, it also set its length to the line length.
	 * 
	 * There is a CLEAR ERROR somewhere as the newly created axis do not have the correct direction.
	 * I don't understand how the transformation matrixes work. and why this do not work. 
	 * TODO: fix this
	 * @param node
	 * @return
	 */
	public Node toAxis(AxisBase node) {
		if (!(node instanceof AxisBase)) {
			return null;
		}		
		TMatrix4d t = new TMatrix4d (getTranslation());
		Matrix3f m = new Matrix3f();
		Math2.getOrthogonalBasis(this.getAxis(), m, true);
		t.setRotationScale(m);
		node.setTransform(t);
		if (node instanceof Axis) {
			((Axis)node).setLength(	getAxis().length());
		}
		return (AxisBase)node;
	}
		
	@Override
	public float[] toFloat() {
		Vector3d v = getTranslation();
		Vector3f a = getAxis();
		return new float[] {(float) v.x, (float) v.y, (float) v.z, a.x, a.y, a.z};
	}
	
	//enh:insert initType ();
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;

	public static final NType.Field idImport$FIELD;

	private static final class _Field extends NType.Field
	{
		private final int id;

		_Field (String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			super (LeafLineImpl.$TYPE, name, modifiers, type, componentType);
			this.id = id;
		}

		@Override
		public void setLong (Object o, long value)
		{
			switch (id)
			{
				case 0:
					((LeafLineImpl) o).idImport = (long) value;
					return;
			}
			super.setLong (o, value);
		}

		@Override
		public long getLong (Object o)
		{
			switch (id)
			{
				case 0:
					return ((LeafLineImpl) o).getIdImport ();
			}
			return super.getLong (o);
		}
	}

	static
	{
		$TYPE = new NType (new LeafLineImpl ());
		$TYPE.addManagedField (idImport$FIELD = new _Field ("idImport", _Field.PROTECTED  | _Field.SCO, de.grogra.reflect.Type.LONG, null, 0));
		initType ();
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new LeafLineImpl ();
	}

	public long getIdImport ()
	{
		return idImport;
	}

	public void setIdImport (long value)
	{
		this.idImport = (long) value;
	}

//enh:end
}

package de.grogra.pointcloud.objects.impl;

import java.util.concurrent.Executor;

import de.grogra.graph.Graph;
import de.grogra.graph.impl.GraphManager;
import de.grogra.graph.impl.Node;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.registry.RegistryContext;
import de.grogra.pf.ui.Command;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.JobManager;
import de.grogra.pf.ui.UI;
import de.grogra.pf.ui.Workbench;
import de.grogra.util.ThreadContext;

/**
 * Not used - should be deleted. This manager was using a separate graphpersistance manager to 
 * import the PC. Then the PC would be copied to the main graph. Currently too slow
 */
public class PointCloudManagerImpl implements RegistryContext {

	private Registry registry;
	//enh:field getter

	@Override
	public Registry getRegistry() {
		return registry;
	}


	public PointCloudManagerImpl(Node root, Context ctx) {
		super();
		initGraph(ctx);
		setRootNode(root);
	}

	public PointCloudManagerImpl setRootNode (Node n) {
		UI.executeLockedly (getGraph(), true,
				new Command ()
		{
			@Override
			public String getCommandName ()
			{
				return null;
			}

			@Override
			public void run (Object arg, Context c)
			{
				GraphManager g = getGraph();
				g.setRoot(Graph.MAIN_GRAPH, n);
			}
		}, null, Workbench.current(), JobManager.ACTION_FLAGS);		
		return this;
	}

	public void initGraph(Context ctx) {
		registry = Registry.create(ctx.getWorkbench().getRegistry().getRootRegistry());
		ThreadContext s = ctx.getWorkbench().getJobManager ().getThreadContext ();
		getGraph().initMainState ((Executor) ctx.getWorkbench().getJobManager ());		
	}

	public GraphManager getGraph() {
		return getRegistry ().getProjectGraph ();
	}



}

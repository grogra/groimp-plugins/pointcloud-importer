package de.grogra.pointcloud.objects.impl;

import static java.lang.Math.max;
import static java.lang.Math.min;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.vecmath.Vector3f;

import de.grogra.graph.EdgePatternImpl;
import de.grogra.graph.Graph;
import de.grogra.graph.impl.Edge;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.Cloud;
import de.grogra.mesh.renderer.handler.CollectionDisplayable;
import de.grogra.mesh.renderer.handler.CollectionMeshHandler;
import de.grogra.mesh.renderer.handler.CollectionPointHandler;
import de.grogra.mesh.renderer.handler.Updatable;
import de.grogra.mesh.utils.PolygonMeshArray;
import de.grogra.persistence.Manageable;
import de.grogra.persistence.PersistenceField;
import de.grogra.persistence.SCOType;
import de.grogra.persistence.Transaction;
import de.grogra.pointcloud.objects.CloudGraphBase;
import de.grogra.pointcloud.objects.CollectionCloud;
import de.grogra.pointcloud.objects.IntermediateCloudNode;
import de.grogra.pointcloud.objects.PointCloudLeaf;
import de.grogra.pointcloud.utils.Visitors.GetBoundariesVisitor;
import de.grogra.pointcloud.utils.Visitors.GetFloatFromPointVisitor;
import de.grogra.pointcloud.utils.Visitors.GetPointVisitor;
import de.grogra.pointcloud.utils.Visitors.RemovePointVisitor;
import de.grogra.pointcloud.utils.Visitors.GetFirstIPCNVisitor;
import de.grogra.pointcloud.utils.Visitors.GetFirstLeafVisitor;
import de.grogra.util.Utils;
import de.grogra.xl.lang.Aggregate;


/**
 * Base implementation of a CloudGraphBase
 */
public class CloudGraph extends CloudGraphBase {

	//enh:sco SCOType

	protected String sourceName;
	//enh:field getmethod=getSourceName

	protected String childrenClass;
	//enh:field getter
	
	transient protected Node cloudNode=null;
	transient private int stamp;
	
	/*
	 * The number of leaf in this graph = number of point in the pointcloud
	 */
	protected int size;
	//enh:field getter hidden
	
	/*
	 * Balancing is done by spreading points to "abstract" intermediate nodes. This 
	 * factor represent the maximum children an intermediate node can have.
	 */
	protected long balancingFactor;
	//enh:field getmethod=getBalancindFactor setter hidden
	
	long depth;
	//enh:field getmethod=getDepth hidden

	transient protected boolean initialized = false; // when loaded the graph display will see the local transfo twice
	transient protected boolean update;
	transient protected boolean boundariesChanged=false;
	private double minx = Double.POSITIVE_INFINITY;
	private double miny = Double.POSITIVE_INFINITY;
	private double minz = Double.POSITIVE_INFINITY;
	private double maxx = Double.NEGATIVE_INFINITY;
	private double maxy = Double.NEGATIVE_INFINITY;
	private double maxz =Double.NEGATIVE_INFINITY; 


	IntermediateCloudNode lastIPCN;
	long minId;
	long maxId;

	public CloudGraph() {
		this.size=0;
		this.sourceName="";
		this.balancingFactor=500;
		this.childrenType=null;
		this.depth=1;
		this.childrenClass="";
	}
	
	
	public CloudGraph(float[] points) {
		this.size=0;
		this.sourceName="";
		this.balancingFactor=500;
		this.childrenType=LeafPointImpl.class;
		this.depth= computeDepthFromNandD(points.length/3, 500) ;
		this.childrenClass="de.grogra.pointcloud.objects.impl.LeafPointImpl";
		setNode(new PointCloudD(this));
		createPathToLeaves(getNode(), 0);
		addPoints(floatsToPoint(points));
		((CollectionDisplayable)getNode()).setDisplayHandler( new CollectionPointHandler() );
	}

	public CloudGraph(String name, long balancingFactor, long depth, Class type) {
		this.size=0;
		this.sourceName=name;
		this.balancingFactor=balancingFactor;
		this.childrenType=type;
		this.depth=depth;
		this.childrenClass=childrenType.getName();
		setNode(new PointCloudD(this));
		// add the first path to the Leaves
		createPathToLeaves(getNode(), 0);
	}

	@Override
	public boolean editable() {
		return true;
	}
	@Override 
	public Node getNode() { 
		return cloudNode; 
		}
	@Override 
	public void setNode(Node n) {
		if (cloudNode != null) {
			for (Edge e = cloudNode.getFirstEdge (); e != null; e = e.getNext (cloudNode))
			{
				if (e.getSource() == cloudNode && e.getTarget() != cloudNode) {
					n.addEdgeBitsTo(e.getTarget(), e.getEdgeBits(), n.getTransaction(false));
					e.remove(n.getTransaction(false));
				}
			}
		} 
		cloudNode = n;
		if (n instanceof CollectionDisplayable) {
			try {
				if (LeafMeshImpl.class.isAssignableFrom(getChildrenType())) {
					((CollectionDisplayable)n).setDisplayHandler(new CollectionMeshHandler() );
				}
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			if (!(((CollectionDisplayable)n).getDisplayHandler() instanceof CollectionMeshHandler)) {
				((CollectionDisplayable)n).setDisplayHandler(new CollectionPointHandler() );
			}
		}
	}

	public String getSourceName ()
	{
		return sourceName;
	}

	@Override
	public void dispose(boolean removeNode) {
		if (getNode() == null) { return; }
		if (getNode() instanceof CollectionDisplayable) {
			((CollectionDisplayable)getNode()).getDisplayHandler().dispose();
		}
		if (removeNode) {
			// if parent is CollectionCloud and this is the last node -> delete it as well.
			if (getNode().getAxisParent() instanceof CollectionCloud) {
				if (((CollectionCloud)getNode().getAxisParent()).getClouds().length==1) {
					getNode().getAxisParent().removeAll(getNode().getGraph().getActiveTransaction());
				}
			}
			getNode().removeAll(getNode().getGraph().getActiveTransaction());	
		} else {
			removeAll(true);
		}
	}
	
	@Override 
	public void dispose() {
		dispose(true);
	}
	
	@Override 
	public Cloud createNew() {
		Cloud c= null;
		try {
			c = new CloudGraph("", getBalancindFactor(), getDepth(), getChildrenType());
			if (getNode()!=null) {
				if (getNode() instanceof CollectionDisplayable) {
					((CollectionDisplayable)getNode()).setDisplayHandler(
							((CollectionDisplayable)getNode()).getDisplayHandler().createNew(c.getNode()));
				}
				c.getNode().setResolution(getNode().getResolution());
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return c;
	}


	/**
	 * create a list of intermediate nodes of the depth required starting from depth d. and add the 
	 * path to "node"
	 * 
	 */
	private void createPathToLeaves(Node node, long d) {
		if (getNode()==null) { return; }
		Node s = node;
		IntermediateCloudNodeImpl t;
		while ((d=d+1)<=depth) {
			t = new IntermediateCloudNodeImpl(d);
			s.addEdgeBitsTo(t, Graph.BRANCH_EDGE, getNode().getTransaction(false));
			s=t;
			this.lastIPCN =t;
		}
	}

	/**
	 * get the depth of the tree based on the number of leaves n, and the degree of the tree
	 * @param n
	 * @param d
	 * @return
	 */
	public static long computeDepthFromNandD(long n, long d) {
		long depth=1;
		while ( (n = n/d+(n%d==0?0:1)) > d) {
			depth++;
		}
		return depth;
	}


	@Override
	public void setPoints(Point[] p) {
		removeAll();
		addPoints(p);
	}

	@Override
	public void addPoints(Point[] nodes) {
		for (Point p : nodes) {
			addPoint(p);
		}
	}

	@Override
	public void addPoint(Point node) {
		if (getNode() == null) { return; }
		if (node.getCloud()!= null) {
			if( node.getCloud() == this) { return; }
			node.getCloud().remove(node);
		}
		
		try {
			if (!(node instanceof PointCloudLeaf) || !getChildrenType().isAssignableFrom(node.getClass())) {
				Point[] leaves = floatsToPoint(node.toFloat());
				for (Point p : leaves) {
					if (!getChildrenType().isAssignableFrom(p.getClass())) { return; }
					addPoint(p);
				}
			}
			else {
				node.setCloud(this);
				PointCloudLeaf n = (PointCloudLeaf) node;
				boolean addedOrDiscarded=false;
				while(!addedOrDiscarded) {
					//while the lastICN is not full > add to him. 
					if (getLastIPCN().getNumberChildren()<balancingFactor) {
						getLastIPCN().addChild(n.getIdImport());
						getLastIPCN().addEdgeBitsTo(n.getNode(), Graph.BRANCH_EDGE, null);
						addedOrDiscarded=true;
						size++;
						updateBounds(n);
						setUpdate(true); //TODO: should it be there?
					} else {
						Node parent = getLastIPCN().getAxisParent();
						if (parent==getNode() && getNode().getDirectChildCount()>balancingFactor) {
							
							lastIPCN = getFirstAvailableIPCN(); // try to get a existing IPCN with room
							if (lastIPCN == null) {
								rebalance();
							}
							if (lastIPCN == null) { // the rebalancing failed
								//Something when wrong - the graph is full but some not are not added
								// for now discard them
								System.out.println("SHOULDN't BE there. Some nodes have been discarded");
								addedOrDiscarded=true;
							}
						} else {
							createPathToLeaves(parent, getLastIPCN().getLevel()-1);
						}
					}
				}
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	private void updateBounds(PointCloudLeaf node) {
		float[] pos = node.toFloat();
		int i = 0;
		if (pos.length<3) { return; }
		for (float f : pos) {
			if (i == 0) {
				minx = min(minx, f);
				maxx = max(maxx, f);
				i=1;
			} else if (i==1) {
				miny = min(miny, f);
				maxy = max(maxy, f);
				i=2;
			} else if (i==2) {
				minz = min(minz, f);
				maxz = max(maxz, f);
				i=0;
			}
		}
	}

	@Override
	public void addPoint(IntermediateCloudNode[] nodes) {
		for (IntermediateCloudNode p : nodes) {
			addPoint(p);
		}
	}

	@Override
	public void addPoint(IntermediateCloudNode node) {
		if (getNode() == null) { return; }
		boolean addedOrDiscared=false;
		while(!addedOrDiscared) {
			//while the lastICN is not full > add to him. 
			if (getLastIPCN().getNumberChildren()<balancingFactor) {
				getLastIPCN().addEdgeBitsTo(node, Graph.BRANCH_EDGE, null);
				addedOrDiscared=true;
				size++;
			} else {
				Node parent = getLastIPCN().getAxisParent();
				if (parent==getNode() && getNode().getDirectChildCount()>balancingFactor) {
					//Something when wrong - the graph is full but some not are not added
					// for now discard them
					System.out.println("SHOULDN't BE there. Some nodes have been discarded");
					addedOrDiscared=true;
				} else {
					createPathToLeaves(parent, getLastIPCN().getLevel()-1);
				}
			}
		}
	}

	/**
	 * args is the set of arguments that the leaf constructor takes in.
	 * e.g. for pointcloudpointimpl it is float x, float y, float z
	 * @param args
	 * @return
	 */
	public PointCloudLeaf createLeaf(Object[] args) {
		Object o=null;
		try {
			o = Utils.invoke (getChildrenType().getName(), args, getChildrenType().getClassLoader());
		} catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException
				| InvocationTargetException e) {
			e.printStackTrace();
		}
		if (o instanceof PointCloudLeaf) {
			return (PointCloudLeaf)o;
		}
		return null;
	}

	public Class<? extends PointCloudLeaf> getChildrenType() throws ClassNotFoundException{
		return (childrenType != null) ? childrenType : 
			(Class<? extends PointCloudLeaf>) Class.forName( this. childrenClass);
	}

	/**
	 * Remove a given node
	 */
	@Override
	public Point remove(Object i) {
		if (!(i instanceof Cloud.Point)) {
			throw new RuntimeException("Cannot remove a non Cloud.Point object from a Cloud.");
		}
		if (!(this.equals(((Cloud.Point)i).getCloud()))) {
			throw new RuntimeException("Point do not belong to this Cloud");
		}
		if (i instanceof Node) {
			((Node)i).removeAll(((Node)i).getGraph().getActiveTransaction());
			
			size--;
			boundariesChanged=true;
			if (size <= 0) {
				// check if the size has been properly updated
				size = getNumberOfPoints();
				if (size <=0) {
					dispose();
				}
			}
			setUpdate(true); //TODO: should it be there?
			return (Point) i;
		}
		throw new RuntimeException("Point not found");
	}

	/**
	 * Remove an array of nodes
	 */
	@Override
	public Point[] remove(Object[] i) {
		Point[] p = new Point[i.length];
		int j = 0;
		for (Object o : i) {
			p[j] = remove(o);
			j++;
		}
		return p;
	}

	/**
	 * This delete all the leaves (all their edges) before delete all edges from 
	 * the pointcloud node to the indtermedate nodes. 
	 * The deletion of the Leaves if required to ensure that no other edges from 
	 * them to another part of the project graph remains.
	 */
	@Override
	public void removeAll() {
		removeAll(true);
	}

	/**
	 * If deep remove is false, only delete the first layer of edges from 
	 * the pointcloud node toward the leaves. If any node of the point 
	 * cloud have other edges toward the project graph, they will remains
	 * persistent through these edges.
	 * 
	 * In most cases this have the same effect as removeall, but if the point
	 * cloud was imported with PLY and some refinement edges exists, the points 
	 * will keep existing after removeall(false);
	 */
	public void removeAll(boolean deepRemove) {
		if (getNode() == null) { return; }
		if (deepRemove) {
			Transaction t = getNode().getTransaction(false);
			if (t == null) {
				Point[] points = getPoints();
				for (Point p : points) {
					if (p instanceof Node) {
						((Node) p).removeAll(t);
					}
				}
			} else {
				RemovePointVisitor v = new RemovePointVisitor(t);
				v.init(getNode().getCurrentGraphState(),  new EdgePatternImpl
						(Graph.BRANCH_EDGE | Graph.SUCCESSOR_EDGE, Graph.REFINEMENT_EDGE, true, true));
				getNode().getGraph().accept(getNode(), v, null);
			}
		} 
			
		for (Edge e = getNode().getFirstEdge (); e != null; e = e.getNext (getNode()))
		{
			if (e.getSource() == getNode() && e.getTarget() != getNode()) {
				e.remove(getNode().getTransaction(false));
			}
		}
		size=0;
	}
	
	/**
	 * Naive "rebalancing" the depth of the graph is incremented, All direct IPCN from the
	 * root are pushed one layer down. (but their layer info is not updated TODO)
	 */
	@Override
	public void rebalance() {
		if (getNode()==null) { return; }
		depth = getDepth() + 1;
		int i = 0;
		for (Edge e = getNode().getFirstEdge (); e != null; e = e.getNext (getNode()))
		{
			if (e.getSource() == getNode() && e.getTarget() != getNode()) {
				if (e.getTarget() instanceof IntermediateCloudNode) {
					IntermediateCloudNode t = new IntermediateCloudNodeImpl(1);
					getNode().addEdgeBitsTo(t, Graph.BRANCH_EDGE, getNode().getTransaction(false));
					t.addEdgeBitsTo(e.getTarget(), Graph.BRANCH_EDGE, getNode().getTransaction(false));
					e.remove(getNode().getTransaction(false));
					i++;
				}
			}
		}
		if (i==0) {
			getNode().addEdgeBitsTo(new IntermediateCloudNodeImpl(1),
					Graph.BRANCH_EDGE, getNode().getTransaction(false));
		}
		lastIPCN = getFirstAvailableIPCN();
	}

	private IntermediateCloudNode getLastIPCN() {
		if (getNode()==null) { return null; }
		if (lastIPCN == null) {
			lastIPCN = getFirstAvailableIPCN();
			if (lastIPCN == null) {
				// if still null create one at depth 0 and map it
				createPathToLeaves(getNode(), (getDepth()>0)?getDepth() :1);
			}
		}
		return lastIPCN;
	}
	
	private IntermediateCloudNode getFirstAvailableIPCN() {
		if (getNode()==null) { return null; }
		GetFirstIPCNVisitor v = new GetFirstIPCNVisitor(balancingFactor);
		v.init(getNode().getCurrentGraphState(),  new EdgePatternImpl
				(Graph.BRANCH_EDGE | Graph.SUCCESSOR_EDGE, Graph.REFINEMENT_EDGE, true, true));
		getNode().getGraph().accept(getNode(), v, null);
		return v.first;
	}

	/**
	 * Here we consider the max degree of the graph as balancing factor
	 */
	public long getBalancindFactor() {
		return balancingFactor;
	}

	public long getDepth() {
		return depth;
	}

	//TODO: probably remove
	public void setMinId(long id) {
		minId=id;
	}
	public void setMaxId(long id) {
		maxId=id;
	}

	/**
	 * Recompute properly the number of children
	 */
	@Override
	public int getNumberOfPoints() {
		if (getNode()==null) { return 0; }
		GetPointVisitor v = new GetPointVisitor();
		v.init(getNode().getCurrentGraphState(),  new EdgePatternImpl
				(Graph.BRANCH_EDGE | Graph.SUCCESSOR_EDGE, Graph.REFINEMENT_EDGE, true, true));
		getNode().getGraph().accept(getNode(), v, null);
		return v.leaves.size();
	}

	/*
	 * Return the first leaf found
	 */
	public PointCloudLeaf getFirstLeaf() {
		if (getNode()==null) { return null; }
		GetFirstLeafVisitor v = new GetFirstLeafVisitor();
		v.init(getNode().getCurrentGraphState(),  new EdgePatternImpl
				(Graph.BRANCH_EDGE | Graph.SUCCESSOR_EDGE, Graph.REFINEMENT_EDGE, true, true));
		getNode().getGraph().accept(getNode(), v, null);
		return v.first;
	}


	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public Point[] getPoints() {
		if (getNode() == null) { return new Point[0]; }
		if (getNode().getGraph() == null) { return new Point[0]; }
		GetPointVisitor v = new GetPointVisitor();
		v.init(getNode().getCurrentGraphState(),  new EdgePatternImpl
				(Graph.BRANCH_EDGE | Graph.SUCCESSOR_EDGE, Graph.REFINEMENT_EDGE, true, true));
		getNode().getGraph().accept(getNode(), v, null);
		return v.leaves.toArray(new PointCloudLeaf[0]);
	}

	protected void recomputeBoundaries() {
		if (!boundariesChanged) { return; }
		if (getNode()==null) { return; }
		boundariesChanged=false;
		GetBoundariesVisitor v = new GetBoundariesVisitor();
		v.init(getNode().getCurrentGraphState(),  new EdgePatternImpl
				(Graph.BRANCH_EDGE | Graph.SUCCESSOR_EDGE, Graph.REFINEMENT_EDGE, true, true));
		getNode().getGraph().accept(getNode(), v, null);
		minx = min(minx, v.minx);
		miny = min(miny, v.miny);
		minz = min(minz, v.minz);
		maxx = max(maxx, v.maxx);
		maxy = max(maxy, v.maxy);
		maxz = max(maxz, v.maxz);
	}

	/**
	 * Should be last but well
	 */
	@Override
	public Point peek() {
		return getFirstLeaf();
	}
	
	@Override
	public Point pop() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void push(Point p) {
		// TODO Auto-generated method stub

	}

	@Override
	public float[] pointsToFloat() {
		if (getNode() == null) { return new float[0]; }
		GetFloatFromPointVisitor v = new GetFloatFromPointVisitor();
		v.init(getNode().getCurrentGraphState(),  new EdgePatternImpl
				(Graph.BRANCH_EDGE | Graph.SUCCESSOR_EDGE, Graph.REFINEMENT_EDGE, true, true));
		getNode().getGraph().accept(getNode(), v, null);

		return v.vertices.toArray();
	}

	@Override
	public Point[] floatsToPoint(float[] points) {
		if (points == null) { return new Point[0]; }
		List<Point> res = new ArrayList<Point>();
		try {
			int i = 0;
			if (LeafPointImpl.class.isAssignableFrom( getChildrenType() ) ) {
				double[] pos = new double[3];
				for (float f : points) {
					if (i == 2) {
						pos[i] = f;
						res.add (new LeafPointImpl(pos) );
						i = 0;
					} else {
						pos[i] = f;
						i++;
					}
				}
			}
			if (LeafLineImpl.class.isAssignableFrom( getChildrenType() ) ) {
				float[] pos = new float[6];
				for (float f : points) {
					if (i == 5) {
						pos[i] = f;
						res.add (new LeafLineImpl(pos[0], pos[1], pos[2], pos[3], pos[4], pos[5]) );
						i = 0;
					} else {
						pos[i] = f;
						i++;
					}
				}
			}
			if (LeafMeshImpl.class.isAssignableFrom( getChildrenType() ) ) {
				float[] pos = new float[9];
				for (float f : points) {
					if (i == 8) {
						pos[i] = f;
						PolygonMeshArray polygonMesh = new PolygonMeshArray();
						polygonMesh.setIndexData(new int[] {0,1,2});
						polygonMesh.setVertexData(pos);
						res.add (new LeafMeshImpl(polygonMesh) );
						i = 0;
					} else {
						pos[i] = f;
						i++;
					}
				}
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return res.toArray(new Point[0]);
	}

	public float getMinimumX() {
		recomputeBoundaries();
		return (float) minx;
	}
	public float getMinimumY() {
		recomputeBoundaries();
		return (float) miny;
	}
	public float getMinimumZ() {
		recomputeBoundaries();
		return (float) minz;
	}
	public float getMaximumX() {
		recomputeBoundaries();
		return (float) maxx;
	}
	public float getMaximumY() {
		recomputeBoundaries();
		return (float) maxy;
	}
	public float getMaximumZ() {
		recomputeBoundaries();
		return (float) maxz;
	}


	public static CloudList create(Aggregate a, Cloud.Point p) {
		if (a.initialize ())
		{
			a.aval1 = new ArrayList<Cloud.Point> ();
		}
		if (a.isFinished ())
		{		
			a.aval = new CloudList();
			((CloudList)a.aval).setPoints( ((ArrayList<Cloud.Point>)a.aval1).toArray(new Cloud.Point[0]) );
		}
		else
		{
			((ArrayList<Cloud.Point>) a.aval1).add (p);
		}
		return null;
	}
	
	@Override

	public Vector3f getCenter() {
		float centerX =this.getMinimumX() + (this.getMaximumX() - this.getMinimumX()) / 2;
		float centerY = this.getMinimumY() + (this.getMaximumY() - this.getMinimumY()) / 2;
		float centerZ = this.getMinimumZ() + (this.getMaximumZ() - this.getMinimumZ()) / 2;
		return new Vector3f(centerX, centerY, centerZ);
	}
	
	@Override
	public Node produceNode() { 
		return ( getNode()!=null)? getNode() : new PointCloudD(this);
	}

	private void setUpdate(boolean b) {
		if (getNode() == null || !(getNode() instanceof Updatable)) {return;}
		((Updatable)getNode()).setUpdate(b);
	}
	
	/**
	 * TODO: this should probably not exists. Find a better way to update the 
	 * size when merging/ spliting clouds
	 */
	public void addSize(int a) {
		size+=a;
	}

	
	/**
	 * Serialization
	 */
	@Override
	public void fieldModified (PersistenceField field, int[] indices, Transaction t)
	{
		stamp++;
	}

	@Override
	public int getStamp ()
	{
		return stamp;
	}

	@Override
	public Manageable manageableReadResolve ()
	{
		return this;
	}
	
	@Override
	public Object manageableWriteReplace ()
	{
		return this;
	}

	@Override
	public void disposeField() {}
	
	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final Type $TYPE;

	public static final Type.Field sourceName$FIELD;
	public static final Type.Field childrenClass$FIELD;
	public static final Type.Field size$FIELD;
	public static final Type.Field balancingFactor$FIELD;
	public static final Type.Field depth$FIELD;

	public static class Type extends SCOType
	{
		public Type (Class c, de.grogra.persistence.SCOType supertype)
		{
			super (c, supertype);
		}

		public Type (CloudGraph representative, de.grogra.persistence.SCOType supertype)
		{
			super (representative, supertype);
		}

		Type (Class c)
		{
			super (c, SCOType.$TYPE);
		}

		private static final int SUPER_FIELD_COUNT = SCOType.FIELD_COUNT;
		protected static final int FIELD_COUNT = SCOType.FIELD_COUNT + 5;

		static Field _addManagedField (Type t, String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			return t.addManagedField (name, modifiers, type, componentType, id);
		}

		@Override
		protected void setInt (Object o, int id, int value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 2:
					((CloudGraph) o).size = (int) value;
					return;
			}
			super.setInt (o, id, value);
		}

		@Override
		protected int getInt (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 2:
					return ((CloudGraph) o).getSize ();
			}
			return super.getInt (o, id);
		}

		@Override
		protected void setLong (Object o, int id, long value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 3:
					((CloudGraph) o).balancingFactor = (long) value;
					return;
				case Type.SUPER_FIELD_COUNT + 4:
					((CloudGraph) o).depth = (long) value;
					return;
			}
			super.setLong (o, id, value);
		}

		@Override
		protected long getLong (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 3:
					return ((CloudGraph) o).getBalancindFactor ();
				case Type.SUPER_FIELD_COUNT + 4:
					return ((CloudGraph) o).getDepth ();
			}
			return super.getLong (o, id);
		}

		@Override
		protected void setObject (Object o, int id, Object value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					((CloudGraph) o).sourceName = (String) value;
					return;
				case Type.SUPER_FIELD_COUNT + 1:
					((CloudGraph) o).childrenClass = (String) value;
					return;
			}
			super.setObject (o, id, value);
		}

		@Override
		protected Object getObject (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					return ((CloudGraph) o).getSourceName ();
				case Type.SUPER_FIELD_COUNT + 1:
					return ((CloudGraph) o).getChildrenClass ();
			}
			return super.getObject (o, id);
		}

		@Override
		public Object newInstance ()
		{
			return new CloudGraph ();
		}

	}

	public de.grogra.persistence.ManageableType getManageableType ()
	{
		return $TYPE;
	}


	static
	{
		$TYPE = new Type (CloudGraph.class);
		sourceName$FIELD = Type._addManagedField ($TYPE, "sourceName", Type.Field.PROTECTED  | Type.Field.SCO, de.grogra.reflect.ClassAdapter.wrap (String.class), null, Type.SUPER_FIELD_COUNT + 0);
		childrenClass$FIELD = Type._addManagedField ($TYPE, "childrenClass", Type.Field.PROTECTED  | Type.Field.SCO, de.grogra.reflect.ClassAdapter.wrap (String.class), null, Type.SUPER_FIELD_COUNT + 1);
		size$FIELD = Type._addManagedField ($TYPE, "size", Type.Field.PROTECTED  | Type.Field.SCO | Type.Field.HIDDEN, de.grogra.reflect.Type.INT, null, Type.SUPER_FIELD_COUNT + 2);
		balancingFactor$FIELD = Type._addManagedField ($TYPE, "balancingFactor", Type.Field.PROTECTED  | Type.Field.SCO | Type.Field.HIDDEN, de.grogra.reflect.Type.LONG, null, Type.SUPER_FIELD_COUNT + 3);
		depth$FIELD = Type._addManagedField ($TYPE, "depth", 0 | Type.Field.SCO | Type.Field.HIDDEN, de.grogra.reflect.Type.LONG, null, Type.SUPER_FIELD_COUNT + 4);
		$TYPE.validate ();
	}

	public int getSize ()
	{
		return size;
	}

	public void setBalancingFactor (long value)
	{
		this.balancingFactor = (long) value;
	}

	public String getChildrenClass ()
	{
		return childrenClass;
	}

//enh:end
}

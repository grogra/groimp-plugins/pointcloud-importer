package de.grogra.pointcloud.objects.impl;

import de.grogra.imp3d.objects.Attributes;
import de.grogra.pointcloud.objects.IntermediateCloudNode;

public class IntermediateCloudNodeImpl extends IntermediateCloudNode {

	public IntermediateCloudNodeImpl() {
		this.maxId=-1;
		this.minId=-1;
		this.level=-1;
		this.numberChildren=0;
	}

	public IntermediateCloudNodeImpl(long level) {
		this.maxId=-1;
		this.minId=-1;
		this.level=level;
		this.numberChildren=0;
	}
	
	private static void initType ()
	{
		$TYPE.addIdentityAccessor (Attributes.SHAPE);
	}
	
	//enh:insert initType ();
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;


	static
	{
		$TYPE = new NType (new IntermediateCloudNodeImpl ());
		initType ();
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new IntermediateCloudNodeImpl ();
	}

//enh:end
}

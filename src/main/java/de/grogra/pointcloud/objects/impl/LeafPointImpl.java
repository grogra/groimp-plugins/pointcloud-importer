package de.grogra.pointcloud.objects.impl;

import java.util.Arrays;

import javax.vecmath.Vector3d;

import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.Attributes;
import de.grogra.imp3d.objects.Null;
import de.grogra.imp3d.objects.Point;
import de.grogra.imp3d.objects.Sphere;
import de.grogra.persistence.ManageableType.Field;
import de.grogra.pointcloud.importer.PointCloudFilterBase;
import de.grogra.pointcloud.objects.PointCloudLeaf;
import de.grogra.util.StringMap;

public class LeafPointImpl extends Point implements PointCloudLeaf {

	protected long idImport;
	//enh:field setter getter
	
	public enum Keys{
		/**
		 * Required X, Y, Z position
		 */
		X,
		Y,
		Z,
		/**
		 * Optional colored arguments
		 */
		RED,
		GREEN,
		BLUE,
		ALPHA
	}

	public LeafPointImpl ()
	{
		super ();
		setLayer (4);
	}
	
	public LeafPointImpl (StringMap args)
	{
		this (getCoordinateFromArgs(args));
		Float[] colors = getColorFromArgs(args);
		if (colors.length==3 && !(Arrays.asList(colors).contains(-1f))) {
			setColor(colors[0], colors[1], colors[2]);
		}
		removeRequiredArgs(args);
		for (String k : args.getKeys()) {
			Field f = this.getNType().getManagedField(k);
			if ( f!=null) {
				f.set (this, new int[] {f.getFieldId()}, args.get(k), null);
			}
		}
	}

	public LeafPointImpl (double x, double y, double z)
	{
		this ();
		setTransform (x, y, z);
	}
	
	/**
	 * 
	 * @param coordinate an array of 3 element in order x,y,z
	 */
	public LeafPointImpl (double[] coordinate)
	{
		this ();
		setTransform (coordinate[0], coordinate[1], coordinate[2]);
	}

	@Override
	public Node getNode() {
		return this;
	}
	
	private static void initType ()
	{
		$TYPE.addIdentityAccessor (Attributes.SHAPE);
	}
	
	private static double[] getCoordinateFromArgs(StringMap args) {
		PointCloudFilterBase filter = (PointCloudFilterBase) args.get("ctx");
		return new double[] {args.getDouble(filter.getKey(Keys.X)), 
			args.getDouble( filter.getKey(Keys.Y)), 
			args.getDouble( filter.getKey(Keys.Z)) };
	}
	
	private static Float[] getColorFromArgs(StringMap args) {
		PointCloudFilterBase filter = (PointCloudFilterBase) args.get("ctx");
		return new Float[] {args.getFloat(filter.getKey(Keys.RED), -1f), 
			args.getFloat( filter.getKey(Keys.GREEN), -1f), 
			args.getFloat( filter.getKey(Keys.BLUE), -1f) };
	}
	
	/**
	 * Delete args values that are linked with "keys" key
	 */
	private void removeRequiredArgs(StringMap args) {
		PointCloudFilterBase filter = (PointCloudFilterBase) args.get("ctx");
		for (Keys k : Keys.values()) {
			args.remove( filter.getKey(k));
		}
	}
	
	public Sphere toSphere(float radius) {
		Sphere s = new Sphere(radius);
		s.setTransform(getTranslation().x, getTranslation().y, getTranslation().z);
		return s;
	}
	
	public Node toNull(Null node) {
		if (!(node instanceof Null)) {
			return null;
		}
		((Null)node).setTransform(getTranslation().x, getTranslation().y, getTranslation().z);
		return (Null)node;
	}

	@Override
	public float[] toFloat() {
		Vector3d v =getTranslation();
		return new float[] {(float)v.x, (float)v.y, (float)v.z};
	}
	
	
	//enh:insert initType ();
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;

	public static final NType.Field idImport$FIELD;

	private static final class _Field extends NType.Field
	{
		private final int id;

		_Field (String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			super (LeafPointImpl.$TYPE, name, modifiers, type, componentType);
			this.id = id;
		}

		@Override
		public void setLong (Object o, long value)
		{
			switch (id)
			{
				case 0:
					((LeafPointImpl) o).idImport = (long) value;
					return;
			}
			super.setLong (o, value);
		}

		@Override
		public long getLong (Object o)
		{
			switch (id)
			{
				case 0:
					return ((LeafPointImpl) o).getIdImport ();
			}
			return super.getLong (o);
		}
	}

	static
	{
		$TYPE = new NType (new LeafPointImpl ());
		$TYPE.addManagedField (idImport$FIELD = new _Field ("idImport", _Field.PROTECTED  | _Field.SCO, de.grogra.reflect.Type.LONG, null, 0));
		initType ();
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new LeafPointImpl ();
	}

	public long getIdImport ()
	{
		return idImport;
	}

	public void setIdImport (long value)
	{
		this.idImport = (long) value;
	}

//enh:end
}
